﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    public interface ISynsetID:IHasPOS
    {
        int Offset { get; }
    }
}
