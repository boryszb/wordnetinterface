﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.Graph
{
    public class Digraph:IDigraph
    {
        private int _vertices;
        private int _edges;
        readonly private Dictionary<int, IList<int>> _adj;

        public Digraph(int size)
        {
           _adj = new Dictionary<int,IList<int>>(size);
        }
       
        
        public int Outdegree(int vertex)
        {
            return _adj[vertex].Count; 
        }

        public IEnumerable<int> AdjacencyList(int vertex)
        {
            return _adj[vertex];
        }

        public void AddEdge(int v, int w)
        {
            AddVertex(v);
            AddVertex(w);
                
            _adj[v].Add(w);
                _edges++;
           
        }

        public void AddVertex(int v) 
        {
            if (!_adj.ContainsKey(v))
            {
                _adj.Add(v, new List<int>());
                _vertices++;
            }
        }
       
        public int NumberOfEdges
        {
            get { return _edges; }
        }

        public int NumberOfVertices
        {
            get { return _vertices; }
        }

        public IEnumerable<int> GetAllVertices()
        {
            return _adj.Keys;
        }
    }
}
