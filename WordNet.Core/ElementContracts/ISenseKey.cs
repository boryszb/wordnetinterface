﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordNet.Core.ElementContracts
{
    // sense_key is an encoding of the word sense.
    // it is represented as: lemma%lex_sense.
    // lex_sense is encoded as: ss_type:lex_filenum:lex_id:head_word:head_id
    // example: bragging%5:00:00:proud:00
    public interface ISenseKey: IHasPOS, IComparable<ISenseKey>
    {
        
        // it is the ASCII text of the word or collocation as found in the WordNet database index file.
        String Lemma { get; }

        // a one digit decimal integer representing the synset type for the sense.
        // synset type is encoded as: 1 -> noun; 2 -> verb; 3 -> adjective; 4 -> adverb; 5 -> adjective satellite
        int SynsetType { get; }
        bool IsAdjectiveSatellite { get; }

        //the lexicographer file containing the synset for the sense
        // the name of the file is encoded as a two digit integer
        ILexFile LexicalFile { get; }

        // a two digit integer that, when appended onto lemma, 
        // uniquely identifies a sense within a lexicographer file
        int LexicalID { get; }

        // the lemma of the first word of the satellite's head synset
        // (only present in adjective satellite synset)
        String HeadWord { get; }

        // a two digit integer that, when appended onto head_word, 
        // uniquely identifies the sense of head word within a lexicographer file
        int HeadID { get; }
        void SetHead(String headLemma, int headLexID);
        bool NeedsHeadSet { get; }
    }
}
