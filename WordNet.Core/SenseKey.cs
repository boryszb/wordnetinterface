﻿using System;
using System.Globalization;
using System.Text;
using WordNet.Core.ElementContracts;
using WordNet.Core.Extensions;

namespace WordNet.Core
{
    // The sense index file lists all of the senses in the WordNet database with each line representing one sense.
    // sense_key is an encoding of the word sense.
    // A sense_key is represented as:
    // lemma % ss_type:lex_filenum:lex_id:head_word:head_id

    // lemma is the ASCII text of the word or collocation as found in the WordNet database index file corresponding to pos 
    // ss_type is a one digit decimal integer representing the synset type (POS) for the sense.
    // lex_filenum is a two digit decimal integer representing the name of the lexicographer file containing the synset for the sense.
    // lex_id is a two digit decimal integer that, when appended onto lemma , uniquely identifies a sense within a lexicographer file.
    // only present if the sense is in an adjective satellite synset:
    // head_word is the lemma of the first word of the satellite's head synset
    // head_id is a two digit decimal integer that uniquely identifies tha sense of head_word.

    public sealed class SenseKey:ISenseKey, IEquatable<SenseKey>
    {
        private readonly String _lemma;
        private readonly int _lexID;
        private readonly POS _pos;
        private readonly bool _isAdjSat;
        private readonly ILexFile _lexFile;

        private bool _isHeadSet;
        private String _headLemma = null;
        private int _headLexID = -1;
        private String _toString;

        // is synset?.PartOfSpeech ok? 
        public SenseKey(String lemma, int lexID, ISynset synset)
            : this(lemma, lexID, synset?.PartOfSpeech, synset.IsAdjectiveSatellite, synset.LexicalFile)
        {
            ;
        }

        public SenseKey(String lemma, int lexID, POS pos, bool isAdjSat, ILexFile lexFile, String originalKey)
            : this(lemma, lexID, pos, isAdjSat, lexFile)
        {
            _toString = originalKey ?? throw new ArgumentNullException();
        }

        public SenseKey(String lemma, int lexID, POS pos, ILexFile lexFile, String headLemma, int headLexID, String originalKey)
            : this(lemma, lexID, pos, (headLemma != null), lexFile)
        {
            if (headLemma == null)
            {
                _isHeadSet = true;
            }
            else
            {
                SetHead(headLemma, headLexID);
            }

            _toString = originalKey ?? throw new ArgumentNullException();
        }

        public SenseKey(String lemma, int lexID, POS pos, bool isAdjSat, ILexFile lexFile)
        {
            if (lemma == null)
                throw new ArgumentNullException(nameof(lemma));

            // all sense key lemmas are in lower case
            // also checks for null
            _lemma = lemma.ToLower(CultureInfo.InvariantCulture);
            _lexID = lexID;
            _pos = pos ?? throw new ArgumentNullException(nameof(pos));
            _isAdjSat = isAdjSat;
            _lexFile = lexFile ?? throw new ArgumentNullException(nameof(lexFile));
            _isHeadSet = !isAdjSat;
        }
        
        public string Lemma
        {
            get { return _lemma; }
        }

        public int SynsetType
        {
            get { return _isAdjSat ? 5 : _pos.Number; }
        }

        public bool IsAdjectiveSatellite
        {
            get { return _isAdjSat; }
        }

        public ILexFile LexicalFile
        {
            get { return _lexFile; }
        }

        public int LexicalID
        {
            get { return _lexID; }
        }

        public string HeadWord
        {
            get { return _headLemma; }
        }

        public int HeadID
        {
            get { return _headLexID; }
        }

        public void SetHead(string headLemma, int headLexID)
        {
            headLexID.CheckLexicalID();

            if (String.IsNullOrWhiteSpace(headLemma))
                throw new ArgumentException(nameof(headLemma));
            _headLemma = headLemma;
            _headLexID = headLexID;
            _isHeadSet = true;
        }

        public bool NeedsHeadSet
        {
            get { return !_isHeadSet; }
        }

        public POS PartOfSpeech
        {
            get { return _pos; }
        }

        public int CompareTo(ISenseKey key)
        {

            key = key ?? throw new ArgumentNullException(nameof(key));
            int cmp;

            // first sort alphabetically by lemma
            cmp = String.Compare(Lemma,key.Lemma, StringComparison.Ordinal);
            if (cmp != 0)
                return cmp;

            // then sort by synset type
            cmp = SynsetType.CompareTo(key.SynsetType);
            if (cmp != 0)
                return cmp;

            // then sort by lex_filenum
            cmp = LexicalFile.Number.CompareTo(key.LexicalFile.Number);
            if (cmp != 0)
                return cmp;

            // then sort by lex_id
            cmp = this.LexicalID.CompareTo(key.LexicalID);
            if (cmp != 0)
                return cmp;

            if (!this.IsAdjectiveSatellite && !key.IsAdjectiveSatellite)
                return 0;
            if (!this.IsAdjectiveSatellite & key.IsAdjectiveSatellite)
                return -1;
            if (this.IsAdjectiveSatellite & !key.IsAdjectiveSatellite)
                return 1;

            // then sort by head_word
            cmp = String.Compare(HeadWord, key.HeadWord,StringComparison.Ordinal);
            if (cmp != 0)
                return cmp;

            // finally by head_id
            return this.HeadID.CompareTo(key.HeadID);
        }

        public override string ToString()
        {
            if (_toString == null)
                _toString = ConvertToString(this);
            return _toString;
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 17;
            result = prime * result + _lemma.GetHashCode();
            result = prime * result + _lexID.GetHashCode();
            result = prime * result + _pos.GetHashCode();
            result = prime * result + _lexFile.GetHashCode();
            result = prime * result + (_isAdjSat ? 1231.GetHashCode() : 1237.GetHashCode());
            if (_isHeadSet)
            {
                result = prime * result + (_headLemma == null ? 0 : _headLemma.GetHashCode());
                result = prime * result + _headLexID.GetHashCode();
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            if (obj is SenseKey other)
            {
                return Equals(other);
            }
            return false;
        }

        public bool Equals(SenseKey other)
        {
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, other))
                return true;

            if (!this._lemma.Equals(other._lemma, StringComparison.Ordinal))
                return false;
            if (this._lexID != other._lexID)
                return false;
            if (this._pos != other._pos)
                return false;
            if (this._lexFile != other._lexFile)
                return false;
            if (this._isAdjSat != other._isAdjSat)
                return false;
            if (this._isAdjSat)
            {
                if (_headLemma != other._headLemma)
                    return false;
                if (_headLexID != other._headLexID)
                    return false;
            }

            return true;
        }

        public static bool operator ==(SenseKey left, SenseKey right)
        {
            if ((object)left == null || (object)right == null)
            {
                return Object.Equals(left, right);
            }

            return left.Equals(right);
        }

        public static bool operator !=(SenseKey left, SenseKey right)
        {
            return !(left == right);
        }
        public static String ConvertToString(ISenseKey key)
        {
            key = key ?? throw new ArgumentNullException(nameof(key));
            // figure out appropriate size
            int size = key.Lemma.Length + 10;
            if (key.IsAdjectiveSatellite)
                size += key.HeadWord.Length + 2;

            // allocate builder
            StringBuilder sb = new StringBuilder(size);

            // make string
            sb.Append(key.Lemma.ToLower(CultureInfo.InvariantCulture));
            sb.Append('%');
            sb.Append(key.SynsetType);
            sb.Append(':');
            sb.Append(key.LexicalFile.Number.GetLexicalFileNumberString());
            sb.Append(':');
            sb.Append(key.LexicalID.GetLexicalIDForSenseKey());
            sb.Append(':');
            if (key.IsAdjectiveSatellite)
            {
                if (key.NeedsHeadSet)
                {
                    sb.Append("??");
                }
                else
                {
                    sb.Append(key.HeadWord);
                }
            }
            sb.Append(':');
            if (key.IsAdjectiveSatellite)
            {
                if (key.NeedsHeadSet)
                {
                    sb.Append("??");
                }
                else
                {
                    sb.Append(key.HeadID.GetLexicalIDForSenseKey());
                }
            }
            return sb.ToString();
        }

        public static bool operator <(SenseKey left, SenseKey right)
        {
            return left is null ? right is object : left.CompareTo(right) < 0;
        }

        public static bool operator <=(SenseKey left, SenseKey right)
        {
            return left is null || left.CompareTo(right) <= 0;
        }

        public static bool operator >(SenseKey left, SenseKey right)
        {
            return left is object && left.CompareTo(right) > 0;
        }

        public static bool operator >=(SenseKey left, SenseKey right)
        {
            return left is null ? right is null : left.CompareTo(right) >= 0;
        }
    }
}
