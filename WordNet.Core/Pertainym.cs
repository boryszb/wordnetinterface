﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class PertainymNode:WordNodeBase, IPertainym
    {
        private readonly List<IWord> _list = new List<IWord>();
        public PertainymNode(IWord word)
        {
            base.Word = word;
        }
       
        public IWord PertainsTo
        {
            get; 
            set;
        }

        public IEnumerable<IWord> Pertains => _list.Select(w => w);

        public void AddWord(IWord word) => _list.Add(word);
    }
}
