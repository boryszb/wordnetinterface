﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.Graph
{
    public interface IFindAncestor
    {
        bool HasPathTo(int vertex);
        int Ancestor { get; }
        int PathLength { get; }
    }
}
