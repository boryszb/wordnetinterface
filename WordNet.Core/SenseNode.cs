﻿using System;
using System.Linq;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class SenseNode:ISenseNode
    {
        readonly ISynset _synset;
        readonly String _words;
        readonly ISenseNode[] _children;
        int N;
        public SenseNode(ISynset synset, int size)
        {
            _synset = synset;
            _children = new ISenseNode[size];
            _words = GetWords();
        }
        public ISynset Synset
        {
            get { return _synset; }
        }

        public string Words
        {
            get { return _words; }
        }

        public int Size
        {
            get { return _children.Length; }
        }

        public ISenseNode this[int i]
        {
            get
            {
                return _children[i];
            }
            set
            {
                _children[i] = value;
                N++;
            }
        }

        public ICollection<ISenseNode> NextNode
        {
            get { return _children.ToArray(); }
        }

        private string GetWords()
        {
           return _synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(','));
        }


        public void Add(ISenseNode node)
        {
            _children[N++] = node;
        }
    }
}
