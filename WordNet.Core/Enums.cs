﻿using System;


namespace WordNet.Core
{
    /// <summary>
    /// Denotes a recognized part of speech
    /// </summary>
    public enum DbPartOfSpeechType
    {
        All = 0,
        Noun = 1,
        Verb = 2,
        Adj = 3,
        Adv = 4,
        Satellite = 5,
        AdjSat = 5,
        NoNaturalPart = 6
    }

    /// <summary>
    /// Denotes the type of database
    /// </summary>
    public enum DbType
    {
        Index = 1,
        Data = 2,
        Exception = 3,
        Sense = 4
    }

    /// <summary>
    /// Denotes the load policy
    /// </summary>
    public enum LoadPolicy
    {
        No_Load = 1,
        Background_Load = 2,
        Immediate_Load = 3
    }


    /// <summary>
    /// Denotes the type of relation
    /// </summary>
    public enum RelationType
    {
        Semantic = 1,
        Lexical = 2,
        Both = 3
    }
}
