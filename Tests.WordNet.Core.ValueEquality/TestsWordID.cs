﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsWordID
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

           Assert.IsTrue(id1 == id2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentSynsetIDAttributeAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(11, POS.NOUN), 2, "test");

            Assert.IsTrue(id1 != id2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentWordNumberAttributeAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 1, "test");

            Assert.IsTrue(id1 != id2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLemmaAttributeAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "tes");

           Assert.IsTrue(id1 != id2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Assert.IsTrue(id1.Equals(id1));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Assert.IsTrue(id1 == id2 && id2 == id1);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id3 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Assert.IsTrue(id1 == id2 && id2 == id3 && id1 == id3);

        }

        [TestMethod]
        public void NonEqualsIsTransitive()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id3 = new WordID(new SynsetID(112, POS.NOUN), 1, "tes");

            Assert.IsTrue(id1 == id2 && id2 != id3 && id1 != id3);

        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 1, "test");

            Assert.AreNotEqual(id1.GetHashCode(), id2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Assert.AreEqual(id1.GetHashCode(), id2.GetHashCode());
        }

    }
}
