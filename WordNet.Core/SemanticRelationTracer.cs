﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class SemanticRelationTracer:ISemanticRelations
    {
        readonly IWNDictionary _dictionary;

        public SemanticRelationTracer(IWNDictionary dic)
        {
            _dictionary = dic;
        }

        public ISenseNode FindRelation(IWordID id, PointerWN pointer)
        {
            if (id == null || pointer == null)
                throw new ArgumentNullException("parameters cannot be null");

            //if (pointer.Relation != RelationType.Semantic)
            //    throw new ArgumentException("It is not semantic pointer");

            if (!IsAccepted(pointer))
                return null;

            ISynset synset = _dictionary.GetSynset(id.SynsetID);
            return FindRelation(synset, pointer);
        }

        public ISenseNode FindRelation(ISynset synset, PointerWN pointer)
        {
            if (synset == null || pointer == null)
                throw new ArgumentNullException("parameters cannot be null");

            //if (pointer.Relation != RelationType.Semantic)
            //    throw new ArgumentException("It is not semantic pointer");

            if (!IsAccepted(pointer))
                return null;

            List<ISynsetID> children = synset.GetRelatedSynsets(pointer);
            if (children == null)
                return null;

            ISenseNode root = new SenseNode(synset, children.Count);
            if (pointer == PointerWN.ATTRIBUTE || pointer == PointerWN.SIMILARTO)
                AddSelfReflexiveNodes(root, pointer);
            else
                AddNodes(root, pointer);
            return root;
        }

        public ISenseNode FindEntailment(IWordID id)
        {
            return FindRelation(id, PointerWN.ENTAILMENT);
        }

        public Dictionary<PointerWN, ISenseNode> FindHolonym(IWordID id)
        {
            ISenseNode result = FindRelation(id, PointerWN.HOLONYMMEMBER);
            Dictionary<PointerWN, ISenseNode> holonyms = new Dictionary<PointerWN, ISenseNode>();
            if (result != null) holonyms.Add(PointerWN.HOLONYMMEMBER, result);

            result = FindRelation(id, PointerWN.HOLONYMPART);
            if (result != null) holonyms.Add(PointerWN.HOLONYMPART, result);

            result = FindRelation(id, PointerWN.HOLONYMSUBSTANCE);
            if (result != null) holonyms.Add(PointerWN.HOLONYMSUBSTANCE, result);

            return holonyms;
        }

        public Dictionary<PointerWN, ISenseNode> FindHypernym(IWordID id)
        {
            ISenseNode result = FindRelation(id, PointerWN.HYPERNYM);
            Dictionary<PointerWN, ISenseNode> hypernyms = new Dictionary<PointerWN, ISenseNode>();
            if (result != null) hypernyms.Add(PointerWN.HYPERNYM, result);

            result = FindRelation(id, PointerWN.HYPERNYMINSTANCE);
            if (result != null) hypernyms.Add(PointerWN.HYPERNYMINSTANCE, result);

            return hypernyms;
        }

        public Dictionary<PointerWN, ISenseNode> FindHyponym(IWordID id)
        {
            ISenseNode result = FindRelation(id, PointerWN.HYPONYM);
            Dictionary<PointerWN, ISenseNode> hyponyms = new Dictionary<PointerWN, ISenseNode>();
            if (result != null) hyponyms.Add(PointerWN.HYPONYM, result);

            result = FindRelation(id, PointerWN.HYPONYMINSTANCE);
            if (result != null) hyponyms.Add(PointerWN.HYPONYMINSTANCE, result);

            return hyponyms;
        }

        public Dictionary<PointerWN, ISenseNode> FindMeronym(IWordID id)
        {
            ISenseNode result = FindRelation(id, PointerWN.MERONYMMEMBER);
            Dictionary<PointerWN, ISenseNode> meronyms = new Dictionary<PointerWN, ISenseNode>();
            if (result != null) meronyms.Add(PointerWN.MERONYMMEMBER, result);

            result = FindRelation(id, PointerWN.MERONYMPART);
            if (result != null) meronyms.Add(PointerWN.MERONYMPART, result);

            result = FindRelation(id, PointerWN.MERONYMSUBSTANCE);
            if (result != null) meronyms.Add(PointerWN.MERONYMSUBSTANCE, result);

            return meronyms;
        }

        public ISenseNode FindSimilarTo(IWordID id)
        {
            return FindRelation(id, PointerWN.SIMILARTO);
        }

        public ISenseNode FindAttribute(IWordID id)
        {
            return FindRelation(id, PointerWN.ATTRIBUTE);
        }

        public ISenseNode FindCause(IWordID id)
        {
            return FindRelation(id, PointerWN.CAUSE);
        }

        private void AddNodes(ISenseNode node, PointerWN pointer)
        {
            if (pointer == PointerWN.HYPERNYM || pointer == PointerWN.HYPERNYMINSTANCE)
            {
                AddNodesHypernym(node);
                return;
            }


            Queue<ISenseNode> q = new Queue<ISenseNode>();
            q.Enqueue(node);

            while (q.Count > 0)
            {
                var temp = q.Dequeue();

                var relatedSynsIds = temp.Synset.GetRelatedSynsets(pointer);
                if (relatedSynsIds == null)
                    continue;
                foreach (var synsetId in relatedSynsIds)
                {
                    ISynset s = _dictionary.GetSynset(synsetId);
                    var children = s.GetRelatedSynsets(pointer);
                    ISenseNode newNode = new SenseNode(s, children == null ? 0 : children.Count);
                    temp.Add(newNode);
                    if (children != null)
                        q.Enqueue(newNode);
                }
            }
        }

        private void AddNodesHypernym(ISenseNode node)
        {
            Queue<ISenseNode> q = new Queue<ISenseNode>();
            q.Enqueue(node);

            while (q.Count > 0)
            {
                var temp = q.Dequeue();

                var relatedSynsIds = temp.Synset.GetRelatedMap().ContainsKey(PointerWN.HYPERNYM) ?
                    temp.Synset.GetRelatedSynsets(PointerWN.HYPERNYM) :
                    temp.Synset.GetRelatedSynsets(PointerWN.HYPERNYMINSTANCE);

                if (relatedSynsIds == null)
                    continue;
                foreach (var synsetId in relatedSynsIds)
                {
                    ISynset s = _dictionary.GetSynset(synsetId);
                    var children = s.GetRelatedMap().ContainsKey(PointerWN.HYPERNYM) ?
                        s.GetRelatedSynsets(PointerWN.HYPERNYM) :
                        s.GetRelatedSynsets(PointerWN.HYPERNYMINSTANCE);
                    ISenseNode newNode = new SenseNode(s, children == null ? 0 : children.Count);
                    temp.Add(newNode);
                    if (children != null)
                        q.Enqueue(newNode);
                }
            }
        }

        private void AddSelfReflexiveNodes(ISenseNode node, PointerWN pointer)
        {
            Queue<ISenseNode> q = new Queue<ISenseNode>();
            Dictionary<ISynsetID, object> visited = new Dictionary<ISynsetID, object>();
            q.Enqueue(node);

            while (q.Count > 0)
            {
                var temp = q.Dequeue();
                if (!visited.ContainsKey(temp.Synset.ID))
                    visited.Add(temp.Synset.ID, null);
                var relatedSynsIds = temp.Synset.GetRelatedSynsets(pointer);
                if (relatedSynsIds == null)
                    continue;
                foreach (var synsetId in relatedSynsIds)
                {
                    if (visited.ContainsKey(synsetId))
                        continue;
                    ISynset s = _dictionary.GetSynset(synsetId);
                    var children = s.GetRelatedSynsets(pointer);
                    ISenseNode newNode = new SenseNode(s, children == null ? 0 : children.Count);
                    temp.Add(newNode);
                    if (children != null)
                        q.Enqueue(newNode);
                }
            }
        }

        public static bool IsAccepted(PointerWN p)
        {
            return p == null ? false : p.Relation == RelationType.Semantic;
        }

    }
}
