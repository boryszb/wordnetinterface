﻿using System;
using System.Globalization;
using System.Text;
using WordNet.Core.ElementContracts;
using WordNet.Core.Extensions;

namespace WordNet.Core
{
    public sealed class WordID:IWordID, IEquatable<WordID>
    {

        public const String wordIDPrefix = "WID-";
        public const String unknownLemma = "?";
        public const String unknownWordNumber = "??";

        private readonly ISynsetID _id;
        private readonly String _lemma;
        private readonly int _num;

        public WordID(int offset, POS pos, int num)
            : this(new SynsetID(offset, pos), num)
        {

        }
        public WordID(int offset, POS pos, String lemma)
            : this(new SynsetID(offset, pos), lemma)
        {

        }
        public WordID(ISynsetID id, int num)
        {
            num.CheckWordNumber();
            _id = id ?? throw new ArgumentNullException();
            _num = num;
            _lemma = null;
        }

        public WordID(ISynsetID id, String lemma)
        {
            if (lemma == null)
                throw new ArgumentNullException(nameof(lemma));

            if (lemma.Trim().Length == 0)
                throw new ArgumentOutOfRangeException(nameof(lemma));
            _id = id ?? throw new ArgumentNullException(nameof(id));
            _num = -1;
            _lemma = lemma.ToLower(CultureInfo.InvariantCulture);
        }

        public WordID(ISynsetID id, int num, String lemma)
        {
            if (String.IsNullOrWhiteSpace(lemma))
                throw new ArgumentException(nameof(lemma));
            num.CheckWordNumber();
            _id = id ?? throw new ArgumentNullException();
            _num = num;
            _lemma = lemma;
        }
        public ISynsetID SynsetID
        {
            get { return _id; }
        }

        public int WordNumber
        {
            get
            {
                return _num;
            }
        }

        public string Lemma
        {
            get { return _lemma; }
        }

        public POS PartOfSpeech
        {
            get { return _id.PartOfSpeech; }
        }

        public override int GetHashCode()
        {
            const int PRIME = 31;
            int result = 17;

            result = PRIME * result + _id.GetHashCode();
            result = PRIME * result + _num.GetHashCode();
            result = PRIME * result + (_lemma == null ? 0 : _lemma.GetHashCode());

            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj is WordID other)
            {
                return Equals(other);
            }

            return false;
        }

        public bool Equals(WordID other)
        {
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, other))
                return true;

            return this._id.Equals(other._id) &&
                this._num == other._num &&
                this._lemma == other._lemma;

        }

        public static bool operator ==(WordID left, WordID right)
        {
            if ((object)left == null || (object)right == null)
            {
                return Object.Equals(left, right);
            }

            return left.Equals(right);
        }

        public static bool operator !=(WordID left, WordID right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(wordIDPrefix);
            sb.Append(_id.Offset.ZeroFillOffset());
            sb.Append('-');
            sb.Append(Char.ToUpper(_id.PartOfSpeech.Tag,CultureInfo.InvariantCulture));
            sb.Append('-');
            sb.Append((_num < 0) ? unknownWordNumber : _num.ZeroFillWordNumber());
            sb.Append('-');
            sb.Append(_lemma ?? unknownLemma);
            return sb.ToString();
        }

        public static IWordID ParseWordID(String value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            if (value.Length < 19)
                throw new ArgumentOutOfRangeException(nameof(value));
            if (!value.StartsWith("WID-",StringComparison.Ordinal))
                throw new ArgumentOutOfRangeException(nameof(value));

            // get synset id
            int offset = int.Parse(value.Substring(4, 12),CultureInfo.InvariantCulture);
            POS pos = POS.GetPartOfSpeech(value[13]);
            ISynsetID id = new SynsetID(offset, pos);

            // get word number
            String numStr = value.Substring(15, 17);
            if (!numStr.Equals(unknownWordNumber,StringComparison.Ordinal))
            {
                int num = int.Parse(numStr,CultureInfo.InvariantCulture);
                return new WordID(id, num);
            }

            // get lemma
            String lemma = value.Substring(18);
            if (lemma.Equals(unknownLemma,StringComparison.Ordinal) || lemma.Length == 0)
                throw new InvalidOperationException();

            return new WordID(id, lemma);
        }
    }
}
