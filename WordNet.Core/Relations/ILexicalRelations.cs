﻿using System;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    // contract for tracing lexical pointers
    public interface ILexicalRelations
    {
        IWordNode Find(IWord word, PointerWN p);
        IAntonym FindAntonym(IWord word);
        IDerived FindDerived(IWord word, PointerWN p);
        IParticiple FindParticiple(IWord word);
        IPertainym FindPertainym(IWord word);
    }
}
