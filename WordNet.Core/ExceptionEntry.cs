﻿using System;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public class ExceptionEntry:ExceptionEntryProxy, IExceptionEntry
    {
        private readonly POS _pos;
        private readonly IExceptionEntryID _id;

        public ExceptionEntry(IExceptionEntryProxy proxy, POS pos)
            : base(proxy)
        {
            _pos = pos ?? throw new ArgumentNullException();
            _id = new ExceptionEntryID(SurfaceForm, pos);
        }

        public ExceptionEntry(String surfaceForm, POS pos, params String[] rootForms)
            : base(surfaceForm, rootForms)
        {

            if (pos == null)
                throw new ArgumentNullException(nameof(pos));
            _id = new ExceptionEntryID(SurfaceForm, pos);
            _pos = pos;
        }

        public override string ToString()
        {
            return $"{base.ToString()}-{_pos.ToString()}";
        }

        public POS PartOfSpeech
        {
            get { return _pos; }
        }

        public IExceptionEntryID ID
        {
            get { return _id; }
        }
    }
}
