﻿using System;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class LexicalRelationTracer: ILexicalRelations
    {
        readonly private IWNDictionary _dictionary;

        public LexicalRelationTracer(IWNDictionary dic)
        {
            _dictionary = dic;
        }


        public static bool IsAccepted(PointerWN p)
        {
            return p == null ? false : p.Relation == RelationType.Lexical;
        }
        public IWordNode Find(IWord word, PointerWN pointer)
        {
            if (word == null || pointer == null)
                throw new ArgumentNullException("parameters cannot be null");

            if (pointer == PointerWN.ANTONYM)
                return FindAntonym(word);
            else if (pointer == PointerWN.DERIVEDFROMADJ || pointer == PointerWN.DERIVATIONALLYRELATED)
                return FindDerived(word, pointer);
            else if (pointer == PointerWN.PERTAINYM)
                return FindPertainym(word);
            else if (pointer == PointerWN.PARTICIPLE)
                return FindParticiple(word);
            else
                throw new ArgumentException("wrong pointer");
        }
               
        public IAntonym FindAntonym(IWord word)
        {
            if (word == null)
                throw new ArgumentNullException();

            var relatedWords = word.GetRelatedWords(PointerWN.ANTONYM);
            
            IAntonym wordNode = null;
            if (relatedWords == null)
            {
                if (word.Synset.IsAdjectiveSatellite)
                {
                    foreach (var id in word.Synset.GetRelatedSynsets())
                    {
                        var viaWord = _dictionary.GetSynset(id);
                        var wr = viaWord.GetWords().Find(w => w.GetRelatedMap().ContainsKey(PointerWN.ANTONYM));
                        if (wr != null)
                        {
                            var awi = wr.GetRelatedWords(PointerWN.ANTONYM);
                            wordNode = new AntonymNode(word)
                            {
                                Antonym = new AntonymNode(_dictionary.GetWord(awi[0])),
                                Indirect = wr
                            };
                            break;
                        }
                    }
                }
            }
            else
            {
                //if (relatedWords.Count > 1)
                //    throw new InvalidOperationException();
                wordNode = new AntonymNode(word);
                for (var i = 0; i < relatedWords.Count; i++ )
                {
      
                var relatedWord = relatedWords[i];
                var antonym = _dictionary.GetWord(relatedWord);
                var antNode = new AntonymNode(antonym);
                if (antonym.Synset.IsAdjectiveHead)
                {
                    foreach (var rs in antonym.Synset.GetRelatedSynsets())
                    {
                        var syns = _dictionary.GetSynset(rs);
                        if (syns.IsAdjectiveSatellite)
                        {
                            antNode.AddRelatedSynsets(syns);
                        }
                    }
                }
                wordNode.Antonyms.Add(antNode);
                }
            }
            return wordNode;
        }

        public IDerived FindDerived(IWord word, PointerWN p)
        {
            if (word == null)
                throw new ArgumentNullException();

            if (word.PartOfSpeech != POS.VERB && word.PartOfSpeech != POS.NOUN && word.PartOfSpeech != POS.ADVERB)
                throw new ArgumentException();

            IDerived derived = null;
            var relatedWord = word.GetRelatedWords(p);
            if (relatedWord != null)
            {
                derived = new DerivedNode(word);
                foreach (var w in relatedWord)
                {
                    derived.AddWord(_dictionary.GetWord(w));
                };
            }

            return derived;
        }

        public IParticiple FindParticiple(IWord word)
        {
            if (word == null)
                throw new ArgumentNullException();
            if (word.PartOfSpeech != POS.ADJECTIVE)
                throw new ArgumentException();
           
            IParticiple participle = null;
            var relatedWord = word.GetRelatedWords(PointerWN.PARTICIPLE);
            if (relatedWord != null)
            {
                participle = new ParticipleNode(word)
                {
                    DerivedFrom = _dictionary.GetWord(relatedWord[0])
                };
            }
             
            return participle;
        }

        public IPertainym FindPertainym(IWord word)
        {
            if (word == null)
                throw new ArgumentNullException();
            if (word.PartOfSpeech != POS.ADJECTIVE)
                throw new ArgumentException();

            IPertainym pertainym = null;
            var relatedWord = word.GetRelatedWords(PointerWN.PERTAINYM);
            if (relatedWord != null)
            {
                pertainym = new PertainymNode(word)
                {
                    PertainsTo = _dictionary.GetWord(relatedWord[0])
                };
            }
            
            return pertainym ;
        }
    }
}
