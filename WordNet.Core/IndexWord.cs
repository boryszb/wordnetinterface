﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public sealed class IndexWord: IIndexWord, IEquatable<IndexWord>
    {
        private readonly IIndexWordID _id;
        private readonly int _tagSenseCount;
        private readonly HashSet<IPointer> _pointers;
        private readonly List<IWordID> _wordIDs;
       
        public IndexWord(String lemma, POS pos, int tagSenseCnt, params IWordID[] words)
            : this(new IndexWordID(lemma, pos), tagSenseCnt, null, words)
        { }

        public IndexWord(String lemma, POS pos, int tagSenseCnt, IPointer[] ptrs, params IWordID[] words)
            : this(new IndexWordID(lemma, pos), tagSenseCnt, ptrs, words)
        { }

        public IndexWord(IIndexWordID id, int tagSenseCnt, params IWordID[] words)
            : this(id, tagSenseCnt, null, words)
        { }

        public IndexWord(IIndexWordID id, int tagSenseCnt, IPointer[] ptrs, params IWordID[] words)
        {
            id = id ?? throw new ArgumentNullException();
            if (tagSenseCnt < 0)
                throw new ArgumentException();
            if (words.Length == 0)
                throw new ArgumentException();
            foreach (IWordID wid in words)
                if (wid == null)
                    throw new ArgumentNullException();

            // do pointers as of v2.3.0
            HashSet<IPointer> pointers;
            if (ptrs == null || ptrs.Length == 0)
            {
                pointers = new HashSet<IPointer>();
            }
            else
            {
                pointers = new HashSet<IPointer>();

                foreach (IPointer p in ptrs)
                    if (p == null)
                    {
                        throw new ArgumentNullException();
                    }
                    else
                    {
                        pointers.Add(p);
                    }
            }

            this._id = id;
            this._tagSenseCount = tagSenseCnt;
            this._wordIDs = new List<IWordID>(words);
            this._pointers = pointers;
        }
        
        public string Lemma
        {
            get { return _id.Lemma; }
            
        }

        public List<IWordID> GetWordIDs()
        {
            return _wordIDs.ToList();
        }

        public int TagSenseCount { get { return _tagSenseCount; } }

        public HashSet<IPointer> GetPointers()
        {
            return new HashSet<IPointer>(_pointers);
        }

        public POS PartOfSpeech
        {
            get { return _id.PartOfSpeech; }
        }

        public IIndexWordID ID
        {
            get { return _id; }
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append('[');
            sb.Append(_id.Lemma);
            sb.Append(" (");
            sb.Append(_id.PartOfSpeech);
            sb.Append(") ");
            for (IEnumerator<IWordID> i = _wordIDs.GetEnumerator(); i.MoveNext(); )
            {
                sb.Append(i.Current.ToString());
                if (i.MoveNext())
                    sb.Append(", ");
            }
            sb.Append(']');
            return sb.ToString();
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 17;
            result = prime * result + _id.GetHashCode();
            result = prime * result + _tagSenseCount.GetHashCode();
            for (int i = 0; i < _wordIDs.Count; i++)
                result = prime * result + _wordIDs[i].GetHashCode();
            foreach (var p in _pointers)
                result = prime * result + p.GetHashCode();
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj is IndexWord other)
            {
                return Equals(other);
            }

            return false;
        }

        public bool Equals(IndexWord other)
        {
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, other))
                return true;

            return this._id.Equals(other.ID) &&
                this._tagSenseCount == other.TagSenseCount &&
                this._wordIDs.SequenceEqual(other._wordIDs) &&
                this._pointers.SequenceEqual(other._pointers);
        }

        public static bool operator ==(IndexWord first, IndexWord second)
        {
            if ((object)first == null || (object)second == null)
            {
                return Object.Equals(first, second);
            }

            return first.Equals(second);
        }

        public static bool operator !=(IndexWord first, IndexWord second)
        {
           return !(first == second);
        }

    }
}
