﻿using System;
using System.ComponentModel.Composition;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace Parsers
{
    [Export(typeof(IParser<ISynset>))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class DataLineParser:IParser<ISynset>
    {
        private DataLineParser() { }
       
        public ISynset Parse(string line)
        {
            char[] Tokenizer = new char[] { ' ' };
            string[] tokens = line.Split(Tokenizer, StringSplitOptions.RemoveEmptyEntries);

            // select handler
            char pos_symbol = tokens[2][0];
            IParserHandler<ISynset> handler = GetHandler(POS.GetPartOfSpeech(pos_symbol));

            return handler.Parse(tokens, line);
        }

       
        
        private IParserHandler<ISynset> GetHandler(POS pos)
        {
            if (pos == POS.VERB)
            {
                return new VerbHandler();
            }
            else if (pos == POS.ADJECTIVE)
            {
                return new AdjectiveHandler();
            }
            else
            {
                return new NounAdverbHandler();
            }
        }
    }
}
