﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordNet.Core;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNetWebApi.Models
{
    public abstract class BaseRelations
    {
        private readonly IWNDictionary _dic;
        private readonly string _rel;
        private readonly PointerWN _pointerType;
        private readonly bool _isaccepted;
        private readonly Dictionary<string, string> _pointers;

        protected BaseRelations(IWNDictionary dic, string rel, Dictionary<string, string> pointers)
        {
            _pointers = pointers;
            _dic = dic;
            _rel = rel;
            _pointerType = PointerWN.GetPointerType(_pointers?[_rel], TypeOfSpeach);
            _isaccepted = _pointers.ContainsKey(_rel);
        }

        public abstract POS TypeOfSpeach { get; }
        public PointerWN PointerType => _pointerType;
        public bool IsAcceptedRelation => _isaccepted;
        protected Dictionary<string, string> Pointers => _pointers;
        protected IWNDictionary Dictionary => _dic;
        protected String Relation => _rel;

        public virtual WordSense FindRelations(string lemma)
        {
            if (LexicalRelationTracer.IsAccepted(PointerType))
            {
                return FindLexicalRelations(lemma);
            }

            if (SenseLexicalTracer.IsAccepted(PointerType))
            {
                return FindBothRelations(lemma);
            }

            return FindRealtionImpl(lemma);
        }

        protected virtual WordSense FindRealtionImpl(string lemma)
        {
            WordSense result = new WordSense();
            var indexWord = Dictionary.FindWord(lemma, TypeOfSpeach);
            if (indexWord == null) return null;

            result.Lemma = indexWord.Lemma;
            result.Pos = indexWord.PartOfSpeech.Name;
            result.Relation = Relation;

            SemanticRelationTracer tracer = new SemanticRelationTracer(Dictionary);
            foreach (var item in indexWord.GetWordIDs())
            {
                var related = tracer.FindRelation(item, PointerType);
                SenseModel model;
                if (result.Relation == WNUtillity.ATTRIBUTE)
                {
                    model = MapToAttribute(related);
                }
                else
                {
                    model = MapToSenseModel(related);
                }
                if (model != null && model.RelatedTo.Any())
                    result.AddSenseNode(model);
            }

            return result;
        }

        protected virtual WordSense FindLexicalRelations(string lemma)
        {
            WordSense result = new WordSense();
            var indexWord = _dic.FindWord(lemma, TypeOfSpeach);
            if (indexWord == null) return null;

            result.Lemma = indexWord.Lemma;
            result.Pos = indexWord.PartOfSpeech.Name;
            result.Relation = Relation;

            LexicalRelationTracer tracer = new LexicalRelationTracer(_dic);
            foreach (var item in indexWord.GetWordIDs())
            {
                var word = _dic.GetWord(item);
                var related = tracer.Find(word, PointerType);
                var model = MapToSenseModel(related);
                if (model != null && model.RelatedTo.Any())
                    result.AddSenseNode(model);
            }

            return result;
        }

        protected virtual WordSense FindBothRelations(string lemma)
        {
            WordSense result = new WordSense();
            var indexWord = _dic.FindWord(lemma, TypeOfSpeach);
            if (indexWord == null) return null;

            result.Lemma = indexWord.Lemma;
            result.Pos = indexWord.PartOfSpeech.Name;
            result.Relation = Relation;

            SenseLexicalTracer tracer = new SenseLexicalTracer(_dic);
            foreach (var item in indexWord.GetWordIDs())
            {
                SenseModel model = null;
                if (_pointerType == PointerWN.ALSOSEE)
                {
                    var word = _dic.GetWord(item);
                    var related = tracer.FindAlsosee(word);
                    model = MapToSenseModel(related);
                }
                else
                {
                    var related = tracer.Find(item, PointerType);
                    model = MapToSenseModel(related);
                }

                if (model != null && model.RelatedTo.Any())
                    result.AddSenseNode(model);
            }

            return result;
        }

        protected virtual SenseModel MapToSenseModel(ISenseLexicalResult node)
        {
            if (node == null) return null;
            SenseModel m = new SenseModel();
            if (node.RelatedSynsets() != null)
            {
                m.Words = node.RelatedSynsets()[0]?.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(','));
                m.Gloss = node.RelatedSynsets()[0]?.Gloss;
                m.Name = node.Lemma;
                m.Description = node.CurrentPointer?.Name;
            }
            else
            {
                m.Words = node.RelatedWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(','));
                m.Gloss = node.RelatedWords()[0].Synset.Gloss;
                m.Name = node.Lemma;
                m.Description = node.CurrentPointer.Name;
            }
            return m;
        }

        protected virtual SenseModel MapToSenseModel(ISenseNode node)
        {
            if (node == null) return null;

            SenseModel m = new SenseModel()
            {
                Gloss = node.Synset.Gloss,
                Description = node.Synset.LexicalFile.Description,
                Name = node.Synset.ID.PartOfSpeech.Name,
                Words = node.Words
            };

            foreach (var item in node.NextNode)
            {
                var temp = MapToSenseModel(item);
                m.AddModel(temp);
            }

            return m;
        }
        protected virtual SenseModel MapToAttribute(ISenseNode node)
        {
            if (node == null) return null;

            SenseModel m = new SenseModel()
            {
                Gloss = node.Synset.Gloss,
                Description = node.Synset.LexicalFile.Description,
                Name = node.Synset.ID.PartOfSpeech.Name,
                Words = node.Words
            };

            foreach (var item in node.NextNode)
            {
                var temp = new SenseModel()
                {
                    Gloss = item.Synset.Gloss,
                    Description = item.Synset.LexicalFile.Description,
                    Name = item.Synset.ID.PartOfSpeech.Name,
                    Words = item.Words
                };

                m.AddModel(temp);
            }
            return m;
        }
        protected virtual SenseModel MapToSenseModel(IWordNode node)
        {
            if (node == null) return null;

            SenseModel m = new SenseModel()
            {
                Gloss = node.Word.Synset.Gloss,
                Description = node.Word.Synset.LexicalFile.Description,
                Name = node.Word.PartOfSpeech.Name,
                Words = node.Word.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(','))
            };

            switch (node)
            {
                case IDerived derived:
                    foreach (var d in derived.DerivedFromList)
                    {
                        m.AddModel(new SenseModel
                        {

                            Description = d.Synset.LexicalFile.Description,
                            Gloss = d.Synset.Gloss,
                            Name = d.PartOfSpeech.Name,
                            Words = d.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                        });
                    }
                    break;
                case IAntonym antonym when antonym.Antonyms.Count > 0:
                    m.AddModel(new SenseModel
                    {
                        Description = antonym.Antonyms.FirstOrDefault()?.Word.Synset.LexicalFile.Description,
                        Gloss = antonym.Antonyms.FirstOrDefault()?.Word.Synset.Gloss,
                        Name = antonym.Antonyms.FirstOrDefault()?.Word.PartOfSpeech.Name,
                        Words = antonym.Antonyms.FirstOrDefault()?.Word.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                    });
                    break;
                case IAntonym antonym when antonym.Antonym != null:
                    m.AddModel(new SenseModel
                    {
                        Description = antonym.Antonym.Word.Synset.LexicalFile.Description,
                        Gloss = antonym.Antonym.Word.Synset.Gloss,
                        Name = antonym.Antonym.Word.PartOfSpeech.Name,
                        Words = antonym.Antonym.Word.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                    });
                    break;
                case IAntonym antonym when antonym.Word != null:
                    m.AddModel(new SenseModel
                    {
                        Description = antonym.Word.Synset.LexicalFile.Description,
                        Gloss = antonym.Word.Synset.Gloss,
                        Name = antonym.Word.PartOfSpeech.Name,
                        Words = antonym.Word.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                    });
                    break;
                case IAlsoSee alsoSee:
                    foreach (var item in alsoSee.See)
                    {
                        m.AddModel(new SenseModel
                        {
                            Description = item.Synset.LexicalFile.Description,
                            Gloss = item.Synset.Gloss,
                            Name = item.PartOfSpeech.Name,
                            Words = item.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                        });
                    }
                    break;
                case IPertainym pertainym:
                    foreach (var item in pertainym.Pertains)
                    {
                        m.AddModel(new SenseModel
                        {
                            Description = item.Synset.LexicalFile.Description,
                            Gloss = item.Synset.Gloss,
                            Name = item.PartOfSpeech.Name,
                            Words = item.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                        });
                    }
                    break;
                case IParticiple participle:
                    m.AddModel(new SenseModel
                    {
                        Description = participle.DerivedFrom.Synset.LexicalFile.Description,
                        Gloss = participle.DerivedFrom.Synset.Gloss,
                        Name = participle.DerivedFrom.PartOfSpeech.Name,
                        Words = participle.DerivedFrom.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w))
                    });
                    break;
            }
            return m;
        }

    }
}