﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    public class MemberModel
    {
        private readonly List<Example> _examples = new List<Example>();
        public string Words { get; set; }
        public string Gloss { get; set; }
        public string Type { get; set; }
        public IEnumerable<Example> Examples => _examples.Select(e => e);
        public void AddExample(Example example) => _examples.Add(example);

    }


    public readonly struct Example : IEquatable<Example>
    {
        private readonly string _words;
        private readonly string _gloss;
        public Example(string words, string gloss)
        {
            _words = words;
            _gloss = gloss;
        }
        public string Words => _words;
        public string Gloss => _gloss;

        public override bool Equals(object obj)
        {
            return obj is Example other && Equals(other);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = 31 * hash + Words.GetHashCode();
            hash = 31 * hash + Gloss.GetHashCode();

            return hash;
        }

        public static bool operator ==(Example left, Example right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Example left, Example right)
        {
            return !(left == right);
        }

        public bool Equals(Example other)
        {
            return other.Words == this.Words && other.Gloss == this.Gloss;
        }
    }
}