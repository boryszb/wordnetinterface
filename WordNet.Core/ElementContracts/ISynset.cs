﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    // Contract for a synset in a data file of WordNet database.
    // Each line in a data file corresponds to a synset with semantic relationl pointers to 
    // related synsents. Pointers are followed by moving from one synset to another.
    // The lines in the data files are in the following format:
    // synset_offset  lex_filenum  ss_type  w_cnt  word  lex_id  [word  lex_id...]  p_cnt  [ptr...]  [frames...]  |   gloss
    // where:
    // -synset_offset is current byte offset in the file;
    // -lex_filenum is two digit number corresponding to the lexicographer file name;
    // -ss_type is one character code indicating the synset type: n -> noun, v -> verb, a -> adjective, s -> adj satellite, r -> adverb
    // -w_cnt is two digit hexadecimal number indicating the number of words in the synset;
    // -word is ASCII form of a word as entered in the synset;
    // -lex_id is one digit hexadecimal number that idetifies a sense within a lexicographer file;
    // -p_cnt is three digit number indicating the number of pointer to other, related synsets;
    // -frames (only in data.verb) is a list of numbers corresponding to the generic verb sentence frames;
    // -gloss is a definition of the synset with one or more example senteces;
    
    public interface ISynset:IHasPOS
    {
        int Offset { get; }
        ILexFile LexicalFile { get; }
        int TypeOfPOS { get; }
        String Gloss { get; }
        List<IWord> GetWords();
        IWord GetWord(int wordNumber);
        bool IsAdjectiveHead { get; }
        bool IsAdjectiveSatellite { get; }
        Dictionary<IPointer, List<ISynsetID>> GetRelatedMap();
        List<ISynsetID> GetRelatedSynsets(IPointer synset);
        List<ISynsetID> GetRelatedSynsets();
        ISynsetID ID { get; }
    }
}
