﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordNet.Core.ElementContracts
{
    public interface IWordID: IHasPOS
    {
        ISynsetID SynsetID { get; }
        int WordNumber { get; }
        String Lemma { get; }
    }
}
