﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    public interface ISenseNode
    {
        ISynset Synset { get; }
        String Words { get; }
        int Size { get; }
        void Add(ISenseNode node);
        ISenseNode this[int i] { get; set; }
        ICollection<ISenseNode> NextNode { get; }
    }
}
