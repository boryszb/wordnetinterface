﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordNet.Core;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNetWebApi.Models
{
    public class NounRelations : BaseRelations
    {
        public NounRelations(IWNDictionary dic, string rel) : base(dic, rel,
            new Dictionary<string, string>(){ { "hypernym", "@" }, { "hyponym", "~" },
            {"member","-" },{"membermeronym", "%m" }, {"substancemeronym","%s" }, {"partmeronym", "%p" },
            {"memberholonym", "#m" }, {"substanceholonym","#s" }, {"partholonym", "#p" },
            { "attribute", "="}, {"derivationallyrelated","+" }, {"antonym", "!" }, {"domain",";" } })
        {
        }

        public override POS TypeOfSpeach => POS.NOUN;
        public IEnumerable<MemberModel> GetMemberModel(string lemma)
        {
            return Impl<MemberModel>(lemma, MapToMemberModel);

        }
        public IEnumerable<DomainModel> GetDomainModel(string lemma)
        {
            return Impl<DomainModel>(lemma, MapToDomainModel);
        }

        private IEnumerable<TModel> Impl<TModel>(string lemma, Func<ISenseLexicalResult, ISynset, TModel> map)
        {
            var indexWord = Dictionary.FindWord(lemma, TypeOfSpeach);
            if (indexWord != null)
            {
                SenseLexicalTracer tracer = new SenseLexicalTracer(Dictionary);
                foreach (var item in indexWord.GetWordIDs())
                {
                    var word = Dictionary.GetWord(item);
                    var related = tracer.Find(item, PointerType);
                    TModel model = map(related, word.Synset);

                    if (model != null)
                        yield return model;
                }
            }
        }

        private DomainModel MapToDomainModel(ISenseLexicalResult related, ISynset rootSynset)
        {
            if (related == null || rootSynset == null) return null;

            DomainModel model = new DomainModel()
            {
                Words = rootSynset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(',')),
                Gloss = rootSynset.Gloss,
                Domain = new Domain()
                {
                    Type = related.CurrentPointer.Name,
                    Words = related.RelatedSynsets()[0]?.GetWords().
                        Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(','))
                }

            };
            return model;
        }

        private MemberModel MapToMemberModel(ISenseLexicalResult related, ISynset rootSynset)
        {
            if (related == null || rootSynset == null) return null;

            MemberModel model = new MemberModel()
            {
                Words = rootSynset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(',')),
                Gloss = rootSynset.Gloss,
                Type = related.CurrentPointer.Name
            };

            foreach (var item in related.RelatedSynsets())
            {
                model.AddExample(new Example(item?.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(',')), item?.Gloss));
            }


            return model;
        }
    }
}