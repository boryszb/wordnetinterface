﻿using System;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    public interface IBothRelations
    {
        ISenseLexicalResult Find(IWordID wordId, PointerWN val);
        ISenseLexicalResult Domain(IWordID wordId);
        ISenseLexicalResult Member(IWordID wordId);
    }
}
