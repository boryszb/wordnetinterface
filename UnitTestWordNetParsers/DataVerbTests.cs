﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Parsers;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace UnitTestWordNetParsers
{
    /*  Verb data file contains information corresponding to synsets with relational pointers resolved to synset offsets.
     *  Each line in the file correspondes to a synset that consists of a list of synonymous words or collocations 
     *  (eg. "fountain pen" , "take in" ), and pointers that describe 
     *  the relations between this synset and other synsets. 
     *  Two kinds of relations are recognized: lexical and semantic. 
     *  Lexical relations hold between word forms; semantic relations hold between word meanings.
     */

    [TestClass]
    public class DataVerbTests
    {
        const string DATA_LINE_VERB = @"01983615 38 v 09 chase 0 chase_after 0 trail 0 tail 0 tag 0 give_chase 0 dog 0 go_after 1 track 0 017 @ 01982625 v 0000 + 05754113 n 0902 + 10559724 n 0901 + 00316685 n 0902 + 00482745 n 0501 ^ 02008778 v 0501 + 10528871 n 0401 + 00316887 n 0402 + 00316685 n 0301 + 10337901 n 0102 + 07777156 n 0101 + 00316340 n 0102 ^ 01984477 v 0101 ~ 01134363 v 0000 ~ 01984348 v 0000 ~ 01985358 v 0000 ~ 01985988 v 0000 03 + 08 00 + 09 00 + 10 00 | go after with the intent to catch; ""The policeman chased the mugger down the alley""; ""the dog chased the rabbit""";

        private IParser<ISynset> Create()
        {
            return (IParser<ISynset>)Activator.CreateInstance(typeof(DataLineParser), true);
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsSynsetType()
        {
            var dataLineParser = Create();
            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            Assert.IsInstanceOfType(synset, typeof(ISynset));
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsSynsetOffset()
        {
            const int EXPECTED_OFFSET = 1983615;
            var dataLineParser = Create();
            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            Assert.AreEqual(EXPECTED_OFFSET, synset.Offset);
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsVerbSyntacticCategory()
        {
            POS EXPECTED_POS = POS.VERB;
            var dataLineParser = Create();
            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            Assert.AreEqual(EXPECTED_POS, synset.PartOfSpeech);
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingLexFileNum_ReturnsSynsetWithLexFileObject()
        {
            const string EXPECTED_NAME = "verb.motion";
            const string EXPECTED_CONTENT = "verbs of walking, flying, swimming";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            Assert.IsTrue(EXPECTED_NAME == synset.LexicalFile.Name &&
                          EXPECTED_CONTENT == synset.LexicalFile.Description);
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingListOfWords_ReturnsAllWords()
        {
            string[] EXPECTED_WORDS = { "chase", "chase_after", "trail", "tail", "tag", "give_chase", "dog", "go_after", "track" };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            Assert.IsTrue(synset.GetWords().Select(w => w.Lemma).SequenceEqual(EXPECTED_WORDS));
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingSemanticPointers_ReturnsAllSemanticPointers()
        {
            int[] EXPECTED_SYNSETS = { 1982625, 1134363, 1984348, 1985358, 1985988 };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            // semantic pointers are between synsets
            Assert.IsTrue(synset.GetRelatedSynsets()
                        .Select(syn => syn.Offset)
                        .SequenceEqual(EXPECTED_SYNSETS));
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingLexicalPointers_ReturnsRelatedWordsForSynsetWord()
        {
            int[] EXPECTED_SYNSETS = { 482745, 2008778 };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            // lexical relation between "dog" (num = 5) and related words in other synsets
            Assert.IsTrue(synset.GetWord(5)
                        .GetRelatedWords()
                        .Select(w => w.SynsetID.Offset)
                        .SequenceEqual(EXPECTED_SYNSETS));
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsVerbFramesObjectsForSynsetWord()
        {
            const int EXPECTED_NUM_FOR_FIRST_WORD_IN_SYNSET = 3;
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);


            Assert.AreEqual(EXPECTED_NUM_FOR_FIRST_WORD_IN_SYNSET, synset.GetWord(1).GetVerbFrames().Count);
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsGloss()
        {
            const string EXPECTED_TEXT = @"go after with the intent to catch; ""The policeman chased the mugger down the alley""; ""the dog chased the rabbit""";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(DATA_LINE_VERB);

            Assert.AreEqual(EXPECTED_TEXT, synset.Gloss);

        }
    }
}
