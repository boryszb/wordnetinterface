﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsSenseEntry
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);

           Assert.IsTrue(entry1 == entry2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentKeyAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("tes", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);

            Assert.IsTrue(entry1 != entry2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentOffsetAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 2221, 3, 1);

            Assert.IsTrue(entry1 != entry2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentSenseNumberAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 1, 1);

            Assert.IsTrue(entry1 != entry2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentTagCountAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 11);

            Assert.IsTrue(entry1 != entry2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
           
            Assert.IsTrue(entry1.Equals(entry1));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);
            
            Assert.IsTrue(entry1 == entry2 && entry2 == entry1);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key3 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);
            SenseEntry entry3 = new SenseEntry(key3, 222, 3, 1);

            Assert.IsTrue(entry1 == entry2 && entry2 == entry3 && entry1 == entry3);

        }

        [TestMethod]
        public void NotEqualsIsTransitive()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key3 = new SenseKey("tes", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);
            SenseEntry entry3 = new SenseEntry(key3, 222, 3, 1);

            Assert.IsTrue(entry1 == entry2 && entry2 != entry3 && entry1 != entry3);

        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 222, 3, 1);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);

            Assert.AreEqual(entry1.GetHashCode(), entry2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            SenseKey key1 = new SenseKey("teste", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseEntry entry1 = new SenseEntry(key1, 2224, 5, 2);
            SenseEntry entry2 = new SenseEntry(key2, 222, 3, 1);

            Assert.AreNotEqual(entry1.GetHashCode(), entry2.GetHashCode());
        }
    }
}
