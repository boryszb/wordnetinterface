﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    // Represents different types of relationships
    // between synsets in a Wordnet dictionary.
    // A pointer to other synset is of the form: pointer_symbol  synset_offset  pos  source/target
    // where :
    // synset_offset is the byte offset of the target synset in the data file corresponding to pos.
    // The source/target field distinguishes lexical and semantic pointers. It contains two two digit hexadecimal numbers.
    // The first two digits indicates the word number in the current synset,
    // the last two digits indicate the word number in the target synset.
    // A value of 0000 meand that pointer is represents semantic relation(such relation is between synsets not individual words).
    // A lexical relation between two words in different synsets is represented by non-zero values in the source and 
    // and target word numbers.
    public interface IPointer
    {
        String Symbol {get; }
        String Name { get; }
        String ShortName { get; }
        RelationType Relation { get; }
    }
}
