﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordNet.Core;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNetWebApi.Models
{
    public class VerbRelations : BaseRelations
    {

        public VerbRelations(IWNDictionary dic, string rel) : base(dic, rel,
            new Dictionary<string, string>(){ {"antonym","!"}, {"hypernym","@"}, {"hyponym", "~"},
                {"entailment","*"}, {"cause", ">"}, {"alsosee", "^" }, {"verbgroup", "$"}, {"derivationallyrelated", "+"},
               {"domain", ";"} })
        { }

        public override POS TypeOfSpeach => POS.VERB;

        protected override WordSense FindRealtionImpl(string lemma)
        {
            WordSense result = new WordSense();
            var indexWord = Dictionary.FindWord(lemma, TypeOfSpeach);
            if (indexWord == null) return null;

            result.Lemma = indexWord.Lemma;
            result.Pos = indexWord.PartOfSpeech.Name;
            result.Relation = Relation;

            SemanticRelationTracer tracer = new SemanticRelationTracer(Dictionary);
            foreach (var item in indexWord.GetWordIDs())
            {
                var related = tracer.FindRelation(item, PointerType);
                SenseModel model = MapToSenseModel(related);
                if (model != null && model.RelatedTo.Any())
                    result.AddSenseNode(model);
            }

            return result;
        }

        private WordSense FindVerbgroup(string lemma)
        {
            WordSense result = new WordSense();
            var indexWord = Dictionary.FindWord(lemma, TypeOfSpeach);
            if (indexWord == null) return null;

            result.Lemma = indexWord.Lemma;
            result.Pos = indexWord.PartOfSpeech.Name;
            result.Relation = Relation;

            SenseLexicalTracer tracer = new SenseLexicalTracer(Dictionary);
            foreach (var item in indexWord.GetWordIDs())
            {
                // var word = _dic.GetWord(item);
                var related = tracer.Find(item, PointerType);
                var word = Dictionary.GetWord(item);
                var model = MapVerbGroupToSenseModel(related, word);
                if (model != null && model.RelatedTo.Any())
                    result.AddSenseNode(model);
            }

            return result;
        }

        protected override WordSense FindBothRelations(string lemma)
        {

            if (PointerType == PointerWN.VERBGROUP)
            {
                return FindVerbgroup(lemma);
            }
            return base.FindBothRelations(lemma);
        }
        
        private SenseModel MapVerbGroupToSenseModel(ISenseLexicalResult node, IWord word)
        {
            if (node == null) return null;

            SenseModel model = new SenseModel()
            {
                Gloss = word.Synset.Gloss,
                Description = word.Synset.LexicalFile.Description,
                Name = word.PartOfSpeech.Name,
                Words = word.Synset.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(','))
            };

            foreach (var item in node.RelatedSynsets())
            {
                SenseModel m = new SenseModel()
                {
                    Words = item.GetWords().Select(w => w.Lemma).Aggregate("", (a, w) => (a + ", " + w), (a) => a.Trim(',')),
                    Gloss = item.Gloss,
                    Name = item.PartOfSpeech.Name,
                    Description = item.LexicalFile.Description
                };

                model.AddModel(m);
            }
            return model;
        }
    }
}