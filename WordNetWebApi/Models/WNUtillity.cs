﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    internal static class WNUtillity
    {
        internal const string NOUN = "noun";
        internal const string VERB = "verb";
        internal const string ADJ = "adjective";
        internal const string ADV = "adverb";
        internal const string MEMBER = "member";
        internal const string DOMAIN = "domain";
        internal const string ATTRIBUTE = "attribute";

        internal const string OutOfRange = "number is out of range";
        internal const string NotSuchWord = "No such word in WordNet";
        internal const string NoData = "Can not find data for the word";
        internal const string NoRealation = "no such relation associated with the word";
        internal const string NullOrWhiteSpace = "value can not be null or white space";

    }
}