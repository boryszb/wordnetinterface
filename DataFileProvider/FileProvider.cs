﻿using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core.DataContract;
using DataFileProvider;

namespace WordNet.Core
{
    public class FileProvider<T> : IDataProvider<T>
    {
        private readonly HashSet<IContentType<T>> _types;
        private Uri _uri;
       // private Dictionary<IContentType<T>, IDataSource> _fileMap;
        private Dictionary<string, IDataSource> _fileMap;
        public FileProvider(Uri uri): this(uri, LoadConfiguration()) { }
        public FileProvider(Uri uri, IEnumerable<IContentType<T>> types)
        {
            if (uri == null)
                throw new ArgumentNullException(nameof(uri));
            if (!types.Any())
                throw new ArgumentOutOfRangeException(nameof(types));
            _uri = uri;
            _types = new HashSet<IContentType<T>>(types);
           
        }
       
        public void Close()
        {
            this._fileMap = null;
        }

        public IDataSource GetSourceForContentType(IContentType<T> type)
        {
            return _fileMap[type?.FileName];
        }

        public ISet<IContentType<T>> GetTypes()
        {
            return _types as ISet<IContentType<T>>;
        }

        public bool IsOpen
        {
            get { return _fileMap != null; }
        }

        public void Open()
        {
            DirectoryInfo directory = new DirectoryInfo(_uri.LocalPath);
            if (!directory.Exists)
                throw new IOException();

            this._fileMap = CreateSourceMap(directory);
        }

        public Uri Source
        {
            get
            {
                return _uri;
            }
            set
            {
                if (IsOpen)
                    throw new NotSupportedException();
                _uri = value;
            }
        }

        protected Dictionary<string, IDataSource> CreateSourceMap(DirectoryInfo dir)
        {
            dir = dir ?? throw new ArgumentNullException(nameof(dir));

            Dictionary<string, IDataSource> result = new Dictionary<string, IDataSource>();
            FileInfo file;
            foreach (IContentType<T> type in _types)
            {
                try
                {
                    file = new FileInfo($"{dir.FullName}/{type.FileName}");
                }
                catch 
                {
                    file = null;
                    continue;
                }
                result.Add(type.FileName, new WordNetFile(file));
            }

            return result;
        }

        static private HashSet<IContentType<T>> LoadConfiguration()
        {
            HashSet<IContentType<T>> types = new HashSet<IContentType<T>>();
            FieldInfo[] fields = typeof(ContentType<T>).GetFields(BindingFlags.Static | BindingFlags.Public);
            for (int i = 0; i < fields.Length; i++)
            {
                if (fields[i].FieldType == typeof(ContentType<T>))
                    types.Add((ContentType<T>)fields[i].GetValue(null));
            }

            return types;
        }
    }
}
