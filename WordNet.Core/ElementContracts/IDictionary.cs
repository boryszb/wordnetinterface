﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core.DataContract;

namespace WordNet.Core.ElementContracts
{
    public interface IWNDictionary
    {
        // Find index word, trying different techniques:
        IIndexWord FindWord(String lemma, POS pos);
        
        //Retrieves the specified index word object from the database. 
        IIndexWord GetIndexWord(String lemma, POS pos);

        //Retrieves the specified index word object from the database. 
        IIndexWord GetIndexWord(IIndexWordID id);

        // Returns all index words of the specified part of speech.
        IEnumerable<IIndexWord> GetAllIndexWords(POS pos);

        // returns index words for all part of speach in dictionary
        Dictionary<POS, IIndexWord> GetAllIndexWordsOf(String lemma);
        
        // Retrieves the word with the specified id from the database.
        IWord GetWord(IWordID id);
       
        // Retrieves the word with the specified sense key from the database.
        IWord GetWord(ISenseKey key);

        // Retrieves the synset with the specified id from the database.
        ISynset GetSynset(ISynsetID id);

        // Returns all synsets of the specified part of speech.
        IEnumerable<ISynset> GetAllSynsets(POS pos);

        // Returns synsets for all part of speach of a word
        Dictionary<POS, List<ISynset>> GetAllSynsetsOf(String lemma);
        
        // Retrieves the sense entry for the specified sense key from the database.
        ISenseEntry GetSenseEntry(ISenseKey key);

        // Returns all sense entries in the dictionary.
        IEnumerable<ISenseEntry> GetAllSenseEntries();

        // Retrieves the exception entry for the specified surface form and part of
        // speech from the database. 
        IExceptionEntry GetExceptionEntry(String surfaceForm, POS pos);
        
        // Retrieves the exception entry for the specified id from the database.
        IExceptionEntry GetExceptionEntry(IExceptionEntryID id);

        //  Returns all exception entries of the specified part of speech.
        IEnumerable<IExceptionEntry> GetAllExceptionEntries(POS pos);

        IWordDefinition GetWordDefinition(string lemma, POS pos);

        IDataProvider<T> GetDataProvider<T>();
    }
}
