﻿using System;
using System.ComponentModel.Composition.Hosting;


namespace WordNet.Core
{
    // plumbing for mef
    public abstract class MefContainer
    {
        public static CompositionContainer Container { get; set; }
    }
}
