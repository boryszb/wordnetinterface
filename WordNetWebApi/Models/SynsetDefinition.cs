﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    public class SynsetDefinition
    {
        public string Description { get; set; }
        public string Type { get; set; }
        public string Gloss { get; set; }
    }
}