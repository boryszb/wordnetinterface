﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    // Unique identifier for an index word.
    // It is used to retrieve a specific index word from WordNet databes.
    public sealed class IndexWordID: IIndexWordID, IEquatable<IndexWordID>
    {
       // private const long serialVersionUID = 4683552775420575309L;
        private readonly Regex _whitespace = new Regex("\\s+");
        private readonly String _lemma;
        private readonly POS _pos;


        public IndexWordID(String lemma, POS pos)
        {
            if (lemma == null)
                throw new ArgumentNullException(nameof(lemma));
            lemma = lemma.ToLower(CultureInfo.InvariantCulture).Trim();
            if (lemma.Length == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(lemma));
            }

            _lemma = _whitespace.Match(lemma).Success ? lemma.Replace(_whitespace.Match(lemma).Value, "_") : lemma;
            _pos = pos ?? throw new ArgumentNullException(nameof(pos));
        }

        public POS PartOfSpeech
        {
            get { return _pos; }
        }

        public string Lemma
        {
            get
            {
                return _lemma;
            }
        }

        public override int GetHashCode()
        {
            const int PRIME = 31;
            int result = 17;
            result = PRIME * result + _lemma.GetHashCode();
            result = PRIME * result + _pos.GetHashCode();
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj is IndexWordID other)
            {
                return Equals(other);
            }

            return false;
        }

        public override String ToString()
        {
            return $"XID-{_lemma}-{_pos.Tag}";
        }

        protected Regex Whitespace => _whitespace;

        public bool Equals(IndexWordID other)
        {
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this._lemma.Equals(other._lemma, StringComparison.OrdinalIgnoreCase) &&
                   this._pos.Equals(other._pos);
        }

        public static bool operator ==(IndexWordID first, IndexWordID second)
        {
            if ((object)first == null || (object)second == null)
            {
                return Object.Equals(first, second);
            }

            return first.Equals(second);
        }

        public static bool operator !=(IndexWordID first, IndexWordID second)
        {
            return !(first == second);
        }
    }
}
