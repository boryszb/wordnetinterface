﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsSynsetID
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            Assert.IsTrue(id1 == id2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentOffsetAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(111, POS.ADJECTIVE);
         
            Assert.IsTrue(id1 != id2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentPOSAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.VERB);
           
            Assert.IsTrue(id1 != id2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentAttributesAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(111, POS.NOUN);

            Assert.IsTrue(id1 != id2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);

            Assert.IsTrue(id1.Equals(id1));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            Assert.IsTrue(id1 == id2 && id2 == id1);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id3 = new SynsetID(1111, POS.ADJECTIVE);

            Assert.IsTrue(id1 == id2 && id2 == id3 && id1 == id3);

        }

        [TestMethod]
        public void NonEqualsIsTransitive()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id3 = new SynsetID(111, POS.ADJECTIVE);

            Assert.IsTrue(id1 == id2 && id2 != id3 && id1 != id3);

        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1112, POS.ADJECTIVE);

            Assert.AreNotEqual(id1.GetHashCode(), id2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            Assert.AreEqual(id1.GetHashCode(), id2.GetHashCode());
        }
    }
}
