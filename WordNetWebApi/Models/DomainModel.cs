﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    public class DomainModel
    {
        public string Words { get; set; }
        public string Gloss { get; set; }
        public Domain Domain { get; set; }
    }

    public class Domain
    {
        public string Type { get; set; }
        public string Words { get; set; }
    }
}