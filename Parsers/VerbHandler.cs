﻿using System;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace Parsers
{
    internal class VerbHandler:NounAdverbHandler, IParserHandler<ISynset>
    {
        protected override void AddFrame(string[] tokens, IWordBuilder[] wordProxies)
        {
            int frame_num, word_num;
            int verbFrameCount = int.Parse(tokens[_index++]);
            IVerbFrame frame;
            for (int i = 0; i < verbFrameCount; i++)
            {
                // Consume '+'
                _index++;
                // Get frame number
                frame_num = int.Parse(tokens[_index++]);
                frame = VerbFrame.GetFrame(frame_num); ;
                // Get word number
                word_num = Convert.ToInt32(tokens[_index++], 16);
                if (word_num > 0)
                    wordProxies[word_num - 1].AddVerbFrame(frame);
                else
                {
                    foreach (IWordBuilder proxy in wordProxies)
                        proxy.AddVerbFrame(frame);
                }
            }
        }
    }
}
