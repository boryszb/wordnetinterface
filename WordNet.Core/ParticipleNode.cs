﻿using System;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class ParticipleNode:WordNodeBase, IParticiple
    {
        public ParticipleNode(IWord word)
        {
            base.Word = word;
        }
        public IWord DerivedFrom
        {
            get;
            set;
        }
    }
}
