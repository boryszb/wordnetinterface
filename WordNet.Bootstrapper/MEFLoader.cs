﻿using System;
using System.ComponentModel.Composition.Hosting;
using WordNet.Core.Morphy;
using WordNet.Core;
using Parsers;
using DataFileProvider;


namespace WordNet.Bootstrapper
{
    public static class MEFLoader
    {
        public static CompositionContainer Init()
        {
            AggregateCatalog catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new AssemblyCatalog(typeof(WNDictionary).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(DataFileContext).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(DataLineParser).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(SimpleStemmer).Assembly));

            CompositionContainer container = new CompositionContainer(catalog);

            return container;
        }
    }
}
