﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsers;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace UnitTestWordNetParsers
{

    /* Adverb index file is an alphabetized list of all adverbs in WordNet. 
  * On each line, following the word, is a list of byte offsets in 
  * the corresponding data file, one for each synset containing the adverb.
  *
  */
    [TestClass]
    public class IndexAdverbTests
    {
        const string ADVERB_LINE = @"abaxially r 1 2 ! \ 1 0 00515646";

        private IParser<IIndexWord> Create()
        {
            return (IParser<IIndexWord>)Activator.CreateInstance(typeof(IndexLineParser), true);
        }

        [TestMethod]
        public void Parse_WithAdverbLine_ReturnsInstanceOfIIndexWord()
        {
            var parser = Create();
            var result = parser.Parse(ADVERB_LINE);

            Assert.IsInstanceOfType(result, typeof(IIndexWord));
        }

        [TestMethod]
        public void Parse_WithAdverbLine_ReturnsLemmaFiled()
        {
            const string EXPECTED_LEMA = "abaxially";
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.AreEqual(EXPECTED_LEMA, result.Lemma);
        }

        [TestMethod]
        public void Parse_WithAdverbLine_ReturnsAdverbSyntacticCategory()
        {
            POS EXPECTED_POS = POS.ADVERB;
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.AreEqual(EXPECTED_POS, result.PartOfSpeech);
        }

        [TestMethod]
        public void Parse_WithAdverbLineContainingPointerList_ReturnsAllElementsOfTheList()
        {
            const int EXPECTED_ELEMENTS_NUMBER = 2;
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.AreEqual(EXPECTED_ELEMENTS_NUMBER, result.GetPointers().Count);
        }

        [TestMethod]
        public void Parse_WithAdverbLineContainingAntonymField_ReturnsAntonymPointer()
        {
            var EXPECTED_POINTER_ANTONYM = PointerWN.ANTONYM;
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_ANTONYM));
        }

        [TestMethod]
        public void Parse_WithAdverbLineContainingDerivedField_ReturnsDerivedPointer()
        {
            var EXPECTED_POINTER_DERIVEDFROMADJ = PointerWN.DERIVEDFROMADJ;
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_DERIVEDFROMADJ));
        }

        [TestMethod]
        public void Parse_WithAdverbLineContainingByteOffsetList_ReturnsWordIDsForAllOffsets()
        {
            const int EXPECTED_OFFSET_NUMBER = 1;
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.AreEqual(EXPECTED_OFFSET_NUMBER, result.GetWordIDs().Count);
        }

        [TestMethod]
        public void Parse_WithAdverbLineContainingByteOffsetList_ReturnsWordIDForFirstOffset()
        {
            int EXPECTED_OFFSET = int.Parse("00515646");
            var parser = Create();

            var result = parser.Parse(ADVERB_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[0].SynsetID.Offset);
        }
    }
}
