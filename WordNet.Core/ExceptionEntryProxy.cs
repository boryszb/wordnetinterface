﻿using System;
using System.Collections.Generic;
using System.Text;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public class ExceptionEntryProxy: IExceptionEntryProxy
    {
        private readonly String _surfaceForm;
        private readonly List<String> _rootForms;
        
        public ExceptionEntryProxy(IExceptionEntryProxy proxy)
        {
            if (proxy == null)
                throw new ArgumentNullException();
            _surfaceForm = proxy.SurfaceForm;
            _rootForms = proxy.GetRootForms();
        }

        public ExceptionEntryProxy(String surfaceForm, params String[] rootForms)
        {
            surfaceForm = surfaceForm ?? throw new ArgumentNullException();
            for (int i = 0; i < rootForms.Length; i++)
            {
                if (rootForms[i] == null)
                    throw new ArgumentNullException();
                rootForms[i] = rootForms[i].Trim();
                if (rootForms[i].Length == 0)
                    throw new ArgumentException();
            }

            _surfaceForm = surfaceForm;
            _rootForms = new List<string>(rootForms);
        }
       
        public string SurfaceForm
        {
            get { return _surfaceForm; }
        }

        public List<string> GetRootForms()
        {
            return new List<string>(_rootForms);
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EXC-");
            sb.Append(_surfaceForm);
            sb.Append('[');
            foreach (var item in _rootForms)
            {
                sb.Append(item);
                sb.Append(',');
            }
            sb.Append(']');
            return sb.ToString();
        }
    }
}
