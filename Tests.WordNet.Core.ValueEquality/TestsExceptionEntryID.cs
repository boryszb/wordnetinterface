﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsExceptionEntryID
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);

           Assert.IsTrue(id1 == id2);
        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentSurfaceFormAttributeAreNotEqual()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("tests", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);

            Assert.IsTrue(id1 != id2);
        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentPOSAttributeAreNotEqual()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.ADJECTIVE);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);

            Assert.IsTrue(id1 != id2);
        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            ExceptionEntryID id = new ExceptionEntryID("test", POS.NOUN);

            Assert.IsTrue(id.Equals(id));
        }

        [TestMethod]
        public void EqualsIsSymetric()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);

            Assert.IsTrue(id1.Equals(id2) && id2.Equals(id1));
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id3 = new ExceptionEntryID("test", POS.NOUN);

            Assert.IsTrue(id1.Equals(id2) && id2.Equals(id3) && id1.Equals(id3));
        }

        [TestMethod]
        public void NonEqualsIsTransitive()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id3 = new ExceptionEntryID("test1", POS.NOUN);

            Assert.IsTrue(id1 == id2 && id2 != id3 && id1 != id3);
        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test", POS.NOUN);

            Assert.AreEqual(id1.GetHashCode(), id2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithDifferenAttributesHaveDifferentHashValue()
        {
            ExceptionEntryID id1 = new ExceptionEntryID("test", POS.NOUN);
            ExceptionEntryID id2 = new ExceptionEntryID("test1", POS.NOUN);

            Assert.AreNotEqual(id1.GetHashCode(), id2.GetHashCode());
        }
    }
}
