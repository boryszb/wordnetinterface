﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    // During WordNet development synsets are organized into forty-five lexicographer files 
    // based on syntactic category and logical groupings. grind(1WN) processes these files 
    // and produces a database suitable for use with the WordNet library.

    // The lexicographer files are not included in the WordNet database package.
    public interface ILexFile : IHasPOS
    {
        // file number(exmple: 01)
        int Number { get; }

        // file name(example: adj.pert)
        String Name { get; }

        // a brife description file's content(example: relational adjectives (pertainyms))
        String Description { get; }
    }
}
