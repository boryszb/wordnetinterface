﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    public interface IWord:IHasPOS, IItem<IWordID>
    {
        String Lemma { get; }
        ISynset Synset { get; }
        ISenseKey SenseKey { get; }
        int LexicalID { get; }
        Dictionary<IPointer, List<IWordID>> GetRelatedMap();
        List<IWordID> GetRelatedWords(IPointer word);
        List<IWordID> GetRelatedWords();
        List<IVerbFrame> GetVerbFrames();
        AdjectiveMarker AdjectiveMarker { get; }
    }
}
