﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;


namespace WordNet.Core
{
    public static class MEFLoader
    {
        public static CompositionContainer Load()
        {
            string directoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            using (DirectoryCatalog dirCatalog = new DirectoryCatalog(directoryPath, "*.dll"))
            {
                CompositionContainer container = new CompositionContainer(dirCatalog);
                return container;
            }
        }
    }
}
