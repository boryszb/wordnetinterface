﻿using System;
using System.Collections.Generic;


namespace WordNet.Core.DataContract
{
    public interface IDataProvider<T>
    {
        Uri Source { get; set; }
        IDataSource GetSourceForContentType(IContentType<T> type);
        ISet<IContentType<T>> GetTypes();
        void Open();
        bool IsOpen { get; }
        void Close();

    }
}
