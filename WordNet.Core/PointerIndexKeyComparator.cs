﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    internal class PointerIndexKeyComparator:IEqualityComparer<IPointer>
    {
        
        public bool Equals(IPointer x, IPointer y)
        {
           return x.Equals(y);
        }

        public int GetHashCode(IPointer obj)
        {
            return obj.GetHashCode(); 
        }
    }
}
