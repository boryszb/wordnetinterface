using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using WordNetWebApi.Areas.HelpPage;
using System.Web.Mvc;

using WordNet.Core;

namespace WordNetWebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(HelpPageConfig.Register);
            AreaRegistration.RegisterAllAreas();
            WNDictionary.Container = WordNet.Bootstrapper.MEFLoader.Init();
        }
    }
}
