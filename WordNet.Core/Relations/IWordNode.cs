﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    public interface IWordNode
    {
        IWord Word { get; }
        String WordLemma { get; }
    }

    public interface IAntonym: IWordNode
    {
        ICollection<ISynset> RelatedSynsets { get; }
        void AddRelatedSynsets(ISynset synset);
        IAntonym Antonym { get; set; }
        void ClearAntonyms();
        void AddRange(IList<IAntonym> antonyms);

        IList<IAntonym> Antonyms { get; }
        IWord Indirect { get; set; }
    }

    public interface IDerived: IWordNode
    {
        IWord DerivedFrom { get; set; }
        void AddWord(IWord word);
        IEnumerable<IWord> DerivedFromList { get; }
    }

    public interface IParticiple: IWordNode
    {
        IWord DerivedFrom { get; set; }
    }

    public interface IPertainym: IWordNode
    {
        IWord PertainsTo { get; set; }
        void AddWord(IWord word);
        IEnumerable<IWord> Pertains { get; }
    }

    public interface IAlsoSee : IWordNode
    {
        void AddWord(IWord word);
        IEnumerable<IWord> See { get; }
    }
}
