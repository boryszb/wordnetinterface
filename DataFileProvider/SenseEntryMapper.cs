﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;
using WordNet.Core;

namespace DataFileProvider
{
    public class SenseEntryMapper:IDataMapper<ISenseEntry>
    {
        readonly IDataProvider<ISenseEntry> _fpSenseEntry;
        public SenseEntryMapper(IDataProvider<ISenseEntry> provider)
        {
            _fpSenseEntry = provider;
        }
        
        public IEnumerable<ISenseEntry> GetAll(POS pos)
        {
            //if (pos == null)
            //    throw new ArgumentNullException(nameof(pos));
            if (!_fpSenseEntry.IsOpen) _fpSenseEntry.Open();
            var content = ContentType<ISenseEntry>.SENSE;
            var parser = content.DataType.GetParser();
            IDataSource file = _fpSenseEntry.GetSourceForContentType(content);

            foreach (var line in file.AllLines())
            {
                yield return parser.Parse(line);
            }
        }

        public ISenseEntry GetByKey(object key)
        {
            if (key == null || !(key is ISenseKey))
                throw new ArgumentOutOfRangeException(nameof(key));
           
            ISenseKey id = (ISenseKey)key;
            if (!_fpSenseEntry.IsOpen) _fpSenseEntry.Open();
            var content = ContentType<ISenseEntry>.SENSE;
            IDataSource file = _fpSenseEntry.GetSourceForContentType(content);

            String line = file.Search(id.ToString()) ?? ((WordNetFile)file).SearchFirstLine(id.ToString());

            return line == null ? null : content.DataType.GetParser().Parse(line);
        }

        public ISenseEntry GetDefinition(string query)
        {
            throw new NotImplementedException();
        }

        public IDataProvider<ISenseEntry> DataProvider
        {
            get { return _fpSenseEntry; }
        }
    }
}
