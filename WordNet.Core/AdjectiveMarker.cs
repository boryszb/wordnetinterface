﻿using System;
using System.Collections.Generic;


namespace WordNet.Core
{
    // represents limitaions on the syntactic position an adjective may have in relation to the noun
    // it modifies
    public sealed class AdjectiveMarker
    {
        public static readonly AdjectiveMarker PREDICATE = new AdjectiveMarker("(p)", "predicate position");
        public static readonly AdjectiveMarker PRENOMINAL = new AdjectiveMarker("(a)", "prenominal (attributive) position");
        public static readonly AdjectiveMarker POSTNOMINAL = new AdjectiveMarker("(ip)", "immediately postnominal position");
        private static readonly HashSet<AdjectiveMarker> _cache = new HashSet<AdjectiveMarker>() { PREDICATE, PRENOMINAL, POSTNOMINAL };

        public static IEnumerable<AdjectiveMarker> Values()
        {
            return _cache;
        }

        private readonly String _symbol;
        private readonly String _description;

        private AdjectiveMarker(String symbol, String description)
        {
            if (symbol == null)
                throw new ArgumentNullException();
            if (description == null)
                throw new ArgumentNullException();
            symbol = symbol.Trim();
            description = description.Trim();
            if (symbol.Length == 0)
                throw new ArgumentException();
            if (description.Length == 0)
                throw new ArgumentException();
            _symbol = symbol;
            _description = description;
        }

        // Returns the adjective marker symbol, appended to the ends of
        // adjective words in the data files
        public String Symbol { get { return _symbol; } }

        // Returns a user-readable description of the type of marker
        public String Description { get { return _description; } }
    }
}
