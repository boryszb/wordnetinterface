﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.Graph
{
    // contract for directed graph
    public interface IDigraph
    {
        int Outdegree(int vertex);
        IEnumerable<int> AdjacencyList(int vertex);
        IEnumerable<int> GetAllVertices();
        void AddEdge(int v, int w);
        void AddVertex(int v);
        int NumberOfEdges { get; }
        int NumberOfVertices { get; }
    }
}
