﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsWord
    {
        // helper function to build instances of Synset Type
        private (Synset, Synset) GetSynsets()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, true, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, true, false, "this is test", builders2, ids2);

            return (synset1, synset2);
        }

        private (Synset, Synset) GetSynsetsWithDifferentPOS()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.NOUN);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, null),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, true, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, true, false, "this is test", builders2, ids2);

            return (synset1, synset2);
        }

        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.IsTrue(word1 == word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentWordIDAttributesAreNotEqual()
        {
            // changed lemma
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "testt");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLexIDAttributesAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            // changed lexID
            Word word1 = new Word(synsets.Item1, id1, 1, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

           Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentAdjMarkerAttributesAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            // changed adjMarker
            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.PREDICATE, null, map2);

            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentVerbFrameAttributesAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            // changed verb frame
            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, new List<IVerbFrame> { VerbFrame.NUM_01 }, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentKeysInMapAttributesAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            // changed pointer
            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ALSOSEE, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, new List<IVerbFrame> { VerbFrame.NUM_01 }, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, new List<IVerbFrame> { VerbFrame.NUM_01 }, map2);

            Assert.IsFalse(Object.ReferenceEquals(word1, word2));
            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentWordIDInMapAttributesAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            // changed word id
            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(221, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, new List<IVerbFrame> { VerbFrame.NUM_01 }, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, new List<IVerbFrame> { VerbFrame.NUM_01 }, map2);

            Assert.IsFalse(Object.ReferenceEquals(word1, word2));
            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentSynsetAttributesAreNotEqual()
        {

            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            // changed POS in synset
            var synsets = GetSynsetsWithDifferentPOS();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, new List<IVerbFrame> { VerbFrame.NUM_01 }, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, null, new List<IVerbFrame> { VerbFrame.NUM_01 }, map2);

            Assert.IsFalse(Object.ReferenceEquals(word1, word2));
            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");


            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };


            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);


            Assert.IsTrue(word1.Equals(word1));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.IsTrue(word1.Equals(word2) && word2.Equals(word1));
        }

        [TestMethod]
        public void NotEqualsIsTransitive()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id3 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map3 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            var synsets = GetSynsets();
            var synsets3 = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);
            Word word3 = new Word(synsets3.Item1, id2, 1, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.IsTrue(word1 == word2 && word2 != word3 && word1 != word3);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id3 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map3 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            var synsets = GetSynsets();
            var synsets3 = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);
            Word word3 = new Word(synsets3.Item1, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.IsTrue(word1 == word2 && word2 == word3 && word1 == word3);
        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.PREDICATE, null, map2);

            Assert.AreNotEqual(word1.GetHashCode(), word2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            WordID id1 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");
            WordID id2 = new WordID(new SynsetID(111, POS.NOUN), 2, "test");

            Dictionary<IPointer, List<IWordID>> map1 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };

            Dictionary<IPointer, List<IWordID>> map2 = new Dictionary<IPointer, List<IWordID>>
            {
                {PointerWN.ANTONYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
                {PointerWN.HYPERNYM, new List<IWordID>{ new WordID(222, POS.NOUN, "test"), new WordID(new SynsetID(111, POS.NOUN), 2, "test") } },
            };
            var synsets = GetSynsets();

            Word word1 = new Word(synsets.Item1, id1, 2, AdjectiveMarker.POSTNOMINAL, null, map1);
            Word word2 = new Word(synsets.Item2, id2, 2, AdjectiveMarker.POSTNOMINAL, null, map2);

            Assert.AreEqual(word1.GetHashCode(), word2.GetHashCode());
        }

    }
}
