﻿using System;
using System.Collections.Generic;


namespace WordNet.Core.DataContract
{
    public interface IDataSource
    {
        String ReadLine(long key);
        String Search(String word);
        String Name { get; }

        IEnumerable<String> AllLines();
    }
}
