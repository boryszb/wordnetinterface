﻿using System;
using System.Collections.Generic;


namespace WordNet.Core.DataContract
{
    public abstract class DataContext : IDataContext
    {
        protected abstract IDataMapper<T> GetDataMapper<T>();
        public abstract IDataProvider<T> GetDataProvider<T>();

        public IEnumerable<T> GetAll<T>(POS pos)
        {
            return GetDataMapper<T>().GetAll(pos);
        }

        public T GetItem<T>(string lemma)
        {
            return GetDataMapper<T>().GetDefinition(lemma);
        }

        public T GetItem<T>(object key)
        {
            return GetDataMapper<T>().GetByKey(key);
        }
    }
}
