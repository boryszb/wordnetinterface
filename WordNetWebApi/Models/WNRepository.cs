﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace WordNetWebApi.Models
{
    public class WNRepository : IWNRepository, IWNMemberRepository, IWNDomainRepository
    {
        readonly private IWNDictionary _dic = new WNDictionary();

        public IEnumerable<DomainModel> GetDomainInfo(string lemma, string pos)
        {
            return new NounRelations(_dic, WNUtillity.DOMAIN).GetDomainModel(lemma);
        }

        public IWordDefinition GetIndexWord(string lemma, string pos)
        {
            return _dic.GetWordDefinition(lemma, POS.GetPartOfSpeech(pos));
        }

        public ILexFile GetLexFile(int num)
        {
            return LexFile.GetLexicalFile(num);
        }

        public IEnumerable<ILexFile> GetLexFiles()
        {
            return LexFile.Values();
        }

        public IEnumerable<MemberModel> GetMemberInfo(string lemma)
        {
            return new NounRelations(_dic, WNUtillity.MEMBER).GetMemberModel(lemma);
        }

        public IEnumerable<WordModel> GetWord(string lemma)
        {
            if (String.IsNullOrWhiteSpace(lemma)) throw new ArgumentException();

            IList<WordModel> result = new List<WordModel>();
            WordModel item;
            foreach (var pos in POS.Values())
            {
                item = GetWord(lemma, pos.Name);
                if (item != null) result.Add(item);
            }

            return result;
        }

        public WordModel GetWord(string lemma, string pos)
        {
            if (String.IsNullOrWhiteSpace(lemma)) throw new ArgumentException(WNUtillity.NullOrWhiteSpace, nameof(lemma));
            if (String.IsNullOrWhiteSpace(pos)) throw new ArgumentException(WNUtillity.NullOrWhiteSpace, nameof(pos));

            var word = _dic.GetWordDefinition(lemma, POS.GetPartOfSpeech(type: pos.ToLowerInvariant()));
            if (word == null) return null;

            WordModel wordDefinition = new WordModel()
            {
                Lemma = word.Lemma,
                TypeOfWord = word.PartOfSpeech.Name
            };

            foreach (var item in word.Synsets)
            {
                wordDefinition.AddSynsetDefinition(new SynsetDefinition
                {
                    Description = item.LexicalFile.Description,
                    Type = item.PartOfSpeech.Name,
                    Gloss = item.Gloss
                });
            }

            return wordDefinition;
        }

        public WordSense GetWordWithRelations(string lemma, string pos, string pointerWN)
        {
            switch (pos) 
            {
                case WNUtillity.NOUN:
                    return new NounRelations(_dic, pointerWN).FindRelations(lemma);
                case WNUtillity.VERB:
                    return new VerbRelations(_dic, pointerWN).FindRelations(lemma);
                case WNUtillity.ADJ:
                    return new AdjectiveRelations(_dic, pointerWN).FindRelations(lemma);
                case WNUtillity.ADV:
                    return new AdverbRelations(_dic, pointerWN).FindRelations(lemma);
                default:
                    throw new ArgumentException();
            }

        }
    }
}