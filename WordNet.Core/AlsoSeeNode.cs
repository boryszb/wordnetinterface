﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class AlsoSeeNode : WordNodeBase, IAlsoSee
    {
        private readonly List<IWord> _list = new List<IWord>();

        public AlsoSeeNode(IWord word)
        {
            base.Word = word;
        }
        public IEnumerable<IWord> See => _list;
        public void AddWord(IWord word) => _list.Add(word);
    }
}
