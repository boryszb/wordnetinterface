﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;
using WordNet.Core.ElementContracts;
using Parsers;
using WordNet.Core.DataContract;

namespace UnitTestWordNetParsers
{

   /* Noun index file is an alphabetized list of all nouns in WordNet. 
    * On each line, following the word, is a list of byte offsets in 
    * the corresponding data file, one for each synset containing the word.
    *
    */
    [TestClass]
    public class IndexNounTests
    {
       
        const string NOUN_LINE = "dog n 7 5 @ ~ #m #p %p 7 1 02064081 09963790 09874274 09740502 07571296 03857710 02685221 ";
       
        private IParser<IIndexWord> Create()
        {
            return (IParser<IIndexWord>)Activator.CreateInstance(typeof(IndexLineParser), true);
        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsInstanceOfIIndexWord()
        {
            var parser = Create();
            var result = parser.Parse(NOUN_LINE);

            Assert.IsInstanceOfType(result, typeof(IIndexWord));
        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsLemmaFiled()
        {
            const string EXPECTED_LEMA = "dog";
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_LEMA, result.Lemma);
        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsNounSyntacticCategory()
        {
            POS EXPECTED_POS = POS.NOUN;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_POS, result.PartOfSpeech);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingPointerList_ReturnsAllElementsOfTheList()
        {
            const int EXPECTED_ELEMENTS_NUMBER = 5;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_ELEMENTS_NUMBER, result.GetPointers().Count);
        }

        [TestMethod]
        public void Parse_WithLineContainingHypernymField_ReturnsHypernymPointer()
        {
            var EXPECTED_POINTER_HYPERNYM = PointerWN.HYPERNYM;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_HYPERNYM));
        }

        [TestMethod]
        public void Parse_WithLineContainingHyponymField_ReturnsHyponymPointer()
        {
            var EXPECTED_POINTER_HYPONYM = PointerWN.HYPONYM;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_HYPONYM));
        }

        [TestMethod]
        public void Parse_WithLineContainingMemberHolonymField_ReturnsMemberHolonymPointer()
        {
            var EXPECTED_POINTER_MEMBERHOLONYM = PointerWN.HOLONYMMEMBER;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_MEMBERHOLONYM));
        }

        [TestMethod]
        public void Parse_WithLineContainingPartHolonymField_ReturnsPartHolonymPointer()
        {
            var EXPECTED_POINTER_PARTHOLONYM = PointerWN.HOLONYMPART;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_PARTHOLONYM));
        }

        [TestMethod]
        public void Parse_WithLineContainingPartMeronymField_ReturnsPartMeronymPointer()
        {
            var EXPECTED_POINTER_PARTMERONYM = PointerWN.MERONYMPART;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_PARTMERONYM));
        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDsForAllOffsets()
        {
            const int EXPECTED_OFFSET_NUMBER = 7;
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET_NUMBER, result.GetWordIDs().Count);
        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForFirstOffset()
        {
            int EXPECTED_OFFSET = int.Parse("02064081");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[0].SynsetID.Offset);

        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForSecondOffset()
        {
            int EXPECTED_OFFSET = int.Parse("09963790");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[1].SynsetID.Offset);

        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForeThirdOffset()
        {
            int EXPECTED_OFFSET = int.Parse("09874274");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[2].SynsetID.Offset);
        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForFourthOffset()
        {
            int EXPECTED_OFFSET = int.Parse("09740502");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[3].SynsetID.Offset);
        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForFifthOffset()
        {
            int EXPECTED_OFFSET = int.Parse("07571296");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[4].SynsetID.Offset);
        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForSixthOffset()
        {
            int EXPECTED_OFFSET = int.Parse("03857710");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[5].SynsetID.Offset);
        }

        [TestMethod]
        public void Parse_WithLineContainingByteOffsetList_ReturnsWordIDForSeventhOffset()
        {
            int EXPECTED_OFFSET = int.Parse("02685221");
            var parser = Create();

            var result = parser.Parse(NOUN_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[6].SynsetID.Offset);
        }
    }
}
