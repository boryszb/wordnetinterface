﻿using System;
using System.Collections.Generic;
using WordNet.Core.DataContract;


namespace WordNet.Core
{
    public interface IDataType<T>
    {
        IParser<T> GetParser();
        ISet<String> GetResourceKeywords();
    }
}
