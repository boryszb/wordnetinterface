﻿using System;

namespace WordNet.Core
{
    public interface IContentType<T>
    {
        IDataType<T> DataType { get; }
        POS PartOfSpeech { get; }
        String FileName { get; }
    }
}
