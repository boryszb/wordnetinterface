﻿using System;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace Parsers
{
    internal class AdjectiveHandler:NounAdverbHandler, IParserHandler<ISynset>
    {
        protected override IWordBuilder[] GetWords(string[] tokens)
        {
            int wordCount = Convert.ToInt32(tokens[_index++], 16);
            String lemma;
            AdjectiveMarker marker;
            int lexID; 
            IWordBuilder[] wordProxies = new WordBuilder[wordCount];
            for (int i = 0; i < wordCount; i++)
            {
                // consume next word
                lemma = tokens[_index++];

                // if it is an adjective, it may be followed by a marker
                marker = null;
                foreach (AdjectiveMarker adjMarker in AdjectiveMarker.Values())
                {
                    if (lemma.EndsWith(adjMarker.Symbol))
                    {
                        marker = adjMarker;
                        lemma = lemma.Substring(0, lemma.Length - adjMarker.Symbol.Length);
                    }
                }

                // parse lex_id
                lexID = Convert.ToInt32(tokens[_index++], 16);

                wordProxies[i] = new WordBuilder(i + 1, lemma, lexID, marker);

            }
            return wordProxies;
        }
    }
}
