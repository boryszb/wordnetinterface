﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class SenseLexicalResult:ISenseLexicalResult
    {
        readonly IWordID _wordId;
        readonly List<ISynset> _relatedSynsets;
        readonly List<IWord> _relatedWords;
        readonly RelationType _currentRelation;
        readonly PointerWN _p;
        
         public SenseLexicalResult(IWordID id, List<ISynset> synsets, PointerWN p)
        {
            _wordId = id;
            _relatedSynsets = synsets;
            _currentRelation = RelationType.Semantic;
            _p = p;
        }
       
        public SenseLexicalResult(IWordID id, List<IWord> words, PointerWN p)
        {
            _wordId = id;
            _relatedWords = words;
            _currentRelation = RelationType.Lexical;
            _p = p;
        }
        
        public string Lemma
        {
            get { return _wordId.Lemma;  }
        }

        public RelationType CurrentRelation
        {
            get { return _currentRelation; }
        }

        public ElementContracts.ISynset[] RelatedSynsets()
        {
            return _relatedSynsets?.ToArray();
        }

        public ElementContracts.IWord[] RelatedWords()
        {
            return _relatedWords?.ToArray();
        }

        public PointerWN CurrentPointer
        {
            get { return _p; }
        }
    }
}
