﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    public class WordSense
    {
        private List<SenseModel> _nodes = new List<SenseModel>();
        public string Lemma { get; set; }
        public string Pos { get; set; }
        public string Relation { get; set; }
        public IEnumerable<SenseModel> Relations => _nodes.Select(n => n);
        public void AddSenseNode(SenseModel node) => _nodes.Add(node);
    }
}