﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:WordNet.Core.FileProvider`1.CreateSourceMap(System.IO.DirectoryInfo)~System.Collections.Generic.Dictionary{WordNet.Core.IContentType{`0},WordNet.Core.DataContract.IDataSource}")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>", Scope = "member", Target = "~M:WordNet.Core.FileProvider`1.CreateSourceMap(System.IO.DirectoryInfo)~System.Collections.Generic.Dictionary{System.String,WordNet.Core.DataContract.IDataSource}")]
