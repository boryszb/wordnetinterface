﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.Graph
{
    // represents a data type for 
    // determining whether a digraph has a directed cycle.
    public interface IDirectedCycle
    {
        bool HasCycle { get; }
        IEnumerable<int> Cycle { get; }

    }
}
