﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    // contract for tracing semantic pointers and hierarchies
    public interface ISemanticRelations
    {
        ISenseNode FindRelation(IWordID id, PointerWN p);
        ISenseNode FindRelation(ISynset synset, PointerWN p);
        ISenseNode FindEntailment(IWordID id);
        Dictionary<PointerWN,ISenseNode> FindHolonym(IWordID id);
        Dictionary<PointerWN,ISenseNode> FindHypernym(IWordID id);
        Dictionary<PointerWN,ISenseNode> FindHyponym(IWordID id);
        Dictionary<PointerWN,ISenseNode> FindMeronym(IWordID id);
        ISenseNode FindSimilarTo(IWordID id);
        ISenseNode FindAttribute(IWordID id);
        ISenseNode FindCause(IWordID id);
    }
}
