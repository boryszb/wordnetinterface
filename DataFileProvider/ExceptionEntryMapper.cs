﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace DataFileProvider
{
    public class ExceptionEntryMapper:IDataMapper<IExceptionEntry>
    {
        const string ArgumentNotValid = "argument is not valid";
        readonly IDataProvider<IExceptionEntryProxy> _fpExceptionEntryProxy;
       
        public ExceptionEntryMapper(IDataProvider<IExceptionEntryProxy> provider)
        {
            _fpExceptionEntryProxy = provider;
        }
        
        public IEnumerable<IExceptionEntry> GetAll(WordNet.Core.POS pos)
        {
            if (pos == null)
                throw new ArgumentNullException(nameof(pos));
            
            if (!_fpExceptionEntryProxy.IsOpen) _fpExceptionEntryProxy.Open();
            var content = GetExceptionContentType(pos);
            var parser = content.DataType.GetParser();
            IDataSource file = _fpExceptionEntryProxy.GetSourceForContentType(content);

            foreach (var entry in file.AllLines())
            {
                var entryObject = parser.Parse(entry);
                yield return new ExceptionEntry(entryObject, pos);
            }
        }

        public IExceptionEntry GetByKey(object key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            if (!(key is IExceptionEntryID))
                throw new ArgumentException(ArgumentNotValid, nameof(key));
            
            IExceptionEntryID id = (IExceptionEntryID)key;
            if (!_fpExceptionEntryProxy.IsOpen) _fpExceptionEntryProxy.Open();
            var content = GetExceptionContentType(id.PartOfSpeech);
            IDataSource file = _fpExceptionEntryProxy.GetSourceForContentType(content);
           
            string line = null;
            if (id.SurfaceForm[0] == 'a')
            {
                line = ((WordNetFile)file).SearchFirstLine(id.SurfaceForm); 
            }
            line = line ?? file.Search(id.SurfaceForm);
            //String line = ((WordNetFile)file).Search2(id.SurfaceForm);
            
            if (line == null)
                return null;

            var proxy = content.DataType.GetParser().Parse(line);
            return new ExceptionEntry(proxy, id.PartOfSpeech);
        }

        public IExceptionEntry GetDefinition(string query)
        {
            throw new NotImplementedException();
        }

        public IDataProvider<IExceptionEntry> DataProvider { get { return (IDataProvider<IExceptionEntry>)_fpExceptionEntryProxy; } }

        static private IContentType<IExceptionEntryProxy> GetExceptionContentType(POS pos)
        {
            pos = pos ?? throw new ArgumentNullException(nameof(pos));

            switch (pos.Name)
            {
                case "noun":
                    return ContentType<IExceptionEntryProxy>.EXCEPTION_NOUN;
                case "verb":
                    return ContentType<IExceptionEntryProxy>.EXCEPTION_VERB;
                case "adverb":
                    return ContentType<IExceptionEntryProxy>.EXCEPTION_ADVERB;
                case "adjective":
                    return ContentType<IExceptionEntryProxy>.EXCEPTION_ADJECTIVE;
                default:
                    throw new ArgumentException();
            }
        }
    }
}
