﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsIndexWordID
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

           Assert.IsTrue(wordID1 == wordID2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLemmaAttributeAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("tes", POS.ADJECTIVE);

            Assert.IsTrue(wordID1 != wordID2);
        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentPOSAttributeAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.NOUN);

            Assert.IsTrue(wordID1 != wordID2);
        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentAttributesAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("testt", POS.NOUN);

            Assert.IsTrue(wordID1 != wordID2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            IndexWordID wordID = new IndexWordID("test", POS.ADJECTIVE);

            Assert.IsTrue(wordID.Equals(wordID));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            Assert.IsTrue(wordID1 == wordID2 && wordID2 == wordID1);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID3 = new IndexWordID("test", POS.ADJECTIVE);

            Assert.IsTrue(wordID1 == wordID2 && wordID2 == wordID3 && wordID1 == wordID3);

        }

        [TestMethod]
        public void NonEqualsIsTransitive()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID3 = new IndexWordID("tzst", POS.ADJECTIVE);

            Assert.IsTrue(wordID1 == wordID2 && wordID2 != wordID3 && wordID1 != wordID3);

        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("tst", POS.ADJECTIVE);

            Assert.AreNotEqual(wordID1.GetHashCode(), wordID2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            Assert.AreEqual(wordID1.GetHashCode(), wordID2.GetHashCode());
        }
    }
}
