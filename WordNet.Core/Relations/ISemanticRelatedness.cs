﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    // Semantic relatedness refers to the degree to which two concepts are related.
    // The semantic relatedness of two wordnet nouns A and B 
    // is the minimum length of any ancestral path between any synset v of A and any synset w of B.
    public interface ISemanticRelatedness 
    {
        IEnumerable<string> Nouns();
        bool IsNoun(string word);
        int Distance(string nounA, string nounB);
        ISynset CommonAcestor(string nounA, string nounB);
        string FindLeastRelated(string[] nouns);
    }
}
