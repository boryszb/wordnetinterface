﻿using System;
using System.Collections.Generic;
using WordNet.Core.Extensions;

namespace WordNet.Core
{
    public class UnknownLexFile : LexFile
    {
        private readonly static Dictionary<int, UnknownLexFile> _cache = new Dictionary<int, UnknownLexFile>();
        protected UnknownLexFile(int num)
            : base(num, "Unknown", "Unknown Lexical File", null)
        { }

        public static UnknownLexFile GetUnknownLexicalFile(int num)
        {
            num.CheckLexicalFileNumber();
            UnknownLexFile result = null;
            if (!_cache.ContainsKey(num))
            {
                result = new UnknownLexFile(num);
                _cache.Add(num, result);
            }
            return result;
        }
    }
}
