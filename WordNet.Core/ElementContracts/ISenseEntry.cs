﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    public interface ISenseEntry:IHasPOS
    {
        ISenseKey SenseKey { get; }
        int Offset { get;  }
        int SenseNumber { get;  }
        int TagCount { get;  }
    }
}
