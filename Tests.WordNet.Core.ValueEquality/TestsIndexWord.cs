﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsIndexWord
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, tagSenseCnt: 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));
            IndexWord word2 = new IndexWord(wordID2, tagSenseCnt: 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));

           Assert.IsTrue(word1 == word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentIndexIDAttributeAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("tes", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, tagSenseCnt: 2,
                 new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                 new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));
            IndexWord word2 = new IndexWord(wordID2, tagSenseCnt: 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));


            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentTagCountAttributeAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, tagSenseCnt: 2,
                 new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                 new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));
            IndexWord word2 = new IndexWord(wordID2, tagSenseCnt: 3,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));

            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentPointersAttributeAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, tagSenseCnt: 2,
                 new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                 new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));
            IndexWord word2 = new IndexWord(wordID2, tagSenseCnt: 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));


            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentWordsAttributeAreNotEqual()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, tagSenseCnt: 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 122, POS.ADJECTIVE, num: 2));
            
            IndexWord word2 = new IndexWord(wordID2, tagSenseCnt: 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PERTAINYM },
                new WordID(offset: 222, POS.ADJECTIVE, num: 1), new WordID(offset: 123, POS.ADJECTIVE, num: 2));

            Assert.IsTrue(word1 != word2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            IndexWordID wordID = new IndexWordID("test", POS.ADJECTIVE);
            IndexWord word = new IndexWord(wordID, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            Assert.IsTrue(word.Equals(word));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word2 = new IndexWord(wordID2, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            Assert.IsTrue(word1 == word2 && word2 == word1);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID3 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word2 = new IndexWord(wordID2, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word3 = new IndexWord(wordID3, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            Assert.IsTrue(word1 == word2 && word2 == word3 && word1 == word3);

        }

        [TestMethod]
        public void NotEqualsIsTransitive()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID3 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word2 = new IndexWord(wordID2, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word3 = new IndexWord(wordID3, 3,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            Assert.IsTrue(word1 == word2 && word2 != word3 && word1 != word3);

        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word2 = new IndexWord(wordID2, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            Assert.AreEqual(word1.GetHashCode(), word2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            IndexWordID wordID1 = new IndexWordID("test", POS.ADJECTIVE);
            IndexWordID wordID2 = new IndexWordID("test", POS.ADJECTIVE);

            IndexWord word1 = new IndexWord(wordID1, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 2));

            IndexWord word2 = new IndexWord(wordID2, 2,
                new PointerWN[] { PointerWN.REGION, PointerWN.PARTICIPLE },
                new WordID(222, POS.ADJECTIVE, 1), new WordID(122, POS.ADJECTIVE, 3));

            Assert.AreNotEqual(word1.GetHashCode(), word2.GetHashCode());
        }


    }
}
