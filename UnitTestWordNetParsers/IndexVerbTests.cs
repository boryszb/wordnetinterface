﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsers;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace UnitTestWordNetParsers
{
    /* Verb index file is an alphabetized list of all verbs in WordNet. 
   * On each line, following the word, is a list of byte offsets in 
   * the corresponding data file, one for each synset containing the verb.
   *
   */
    [TestClass]
    public class IndexVerbTests
    {
        const string VERB_LINE = "dog v 1 2 @ ~ 1 1 01983615";

        private IParser<IIndexWord> Create()
        {
            return (IParser<IIndexWord>)Activator.CreateInstance(typeof(IndexLineParser), true);
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsInstanceOfIIndexWord()
        {
            var parser = Create();
            var result = parser.Parse(VERB_LINE);

            Assert.IsInstanceOfType(result, typeof(IIndexWord));
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsLemmaFiled()
        {
            const string EXPECTED_LEMA = "dog";
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.AreEqual(EXPECTED_LEMA, result.Lemma);
        }

        [TestMethod]
        public void Parse_WithVerbLine_ReturnsVerbSyntacticCategory()
        {
            POS EXPECTED_POS = POS.VERB;
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.AreEqual(EXPECTED_POS, result.PartOfSpeech);
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingPointerList_ReturnsAllElementsOfTheList()
        {
            const int EXPECTED_ELEMENTS_NUMBER = 2;
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.AreEqual(EXPECTED_ELEMENTS_NUMBER, result.GetPointers().Count);
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingHypernymField_ReturnsHypernymPointer()
        {
            var EXPECTED_POINTER_HYPERNYM = PointerWN.HYPERNYM;
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_HYPERNYM));
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingHyponymField_ReturnsHyponymPointer()
        {
            var EXPECTED_POINTER_HYPONYM = PointerWN.HYPONYM;
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_HYPONYM));
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingByteOffsetList_ReturnsWordIDsForAllOffsets()
        {
            const int EXPECTED_OFFSET_NUMBER = 1;
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.AreEqual(EXPECTED_OFFSET_NUMBER, result.GetWordIDs().Count);
        }

        [TestMethod]
        public void Parse_WithVerbLineContainingByteOffsetList_ReturnsWordIDForFirstOffset()
        {
            int EXPECTED_OFFSET = int.Parse("01983615");
            var parser = Create();

            var result = parser.Parse(VERB_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[0].SynsetID.Offset);
        }
    }
}
