﻿using System;
using System.Collections.Generic;


namespace WordNet.Core.DataContract
{
    public interface IDataContext
    {
        IEnumerable<T> GetAll<T>(POS pos);
        T GetItem<T>(string lemma);
        T GetItem<T>(object key);
        IDataProvider<T> GetDataProvider<T>();
    }
}
