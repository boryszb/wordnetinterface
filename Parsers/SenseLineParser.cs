﻿using System;
using System.ComponentModel.Composition;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace Parsers
{
    [Export(typeof(IParser<ISenseEntry>))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class SenseLineParser: IParser<ISenseEntry>
    {
        private readonly IParser<ISenseKey> _keyParser;
        private SenseLineParser()
            : this(SenseKeyParser.Instance)
        {
        }

        private SenseLineParser(IParser<ISenseKey> keyParser)
        {
            _keyParser = keyParser ?? throw new ArgumentNullException();
        }

        public ISenseEntry Parse(string line)
        {
            if (line == null)
                throw new ArgumentNullException();
            try
            {
                int begin = 0, end = 0;

                // get sense key
                end = line.IndexOf(' ', begin);
                String keyStr = line.Substring(begin, end);
                ISenseKey sense_key = _keyParser.Parse(keyStr);

                // get offset
                begin = end + 1;
                end = line.IndexOf(' ', begin);
                int synset_offset = int.Parse(line.Substring(begin, end - begin));

                // get sense number
                begin = end + 1;
                end = line.IndexOf(' ', begin);
                int sense_number = int.Parse(line.Substring(begin, end - begin));

                // get tag cnt
                begin = end + 1;
                int tag_cnt = int.Parse(line.Substring(begin));

                return new SenseEntry(sense_key, synset_offset, sense_number, tag_cnt);
            }
            catch
            {
                throw;
            }
        }
    }
}
