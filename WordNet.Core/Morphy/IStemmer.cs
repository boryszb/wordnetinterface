﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Morphy
{
    public interface IStemmer
    {
        List<string> FindStems(string surfaceForm, POS pos);
    }
}
