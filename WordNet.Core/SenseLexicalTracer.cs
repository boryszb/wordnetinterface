﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class SenseLexicalTracer:IBothRelations
    {
        readonly IWNDictionary _dictionary;

        public SenseLexicalTracer(IWNDictionary dic)
        {
            _dictionary = dic;
        }
        
        public ISenseLexicalResult Find(IWordID wordId, PointerWN pointer)
        {
            if (pointer == null)
                throw new ArgumentNullException(nameof(pointer));
            if (wordId == null)
                throw new ArgumentNullException(nameof(wordId));

            if (!IsAccepted(pointer))
                 throw new ArgumentException("it is not correct relation");

            if (pointer == PointerWN.DOMAIN)
                return Domain(wordId);

            if (pointer == PointerWN.MEMBER)
                return Member(wordId);

            var result = FindSemantic(wordId, pointer);
            if (result == null)
                result = FindLexical(wordId, pointer);
            return result;
        }

        public ISenseLexicalResult Domain(IWordID wordId)
        {
            if (wordId == null)
                throw new ArgumentNullException();

            var resut = FindSemantic(wordId, PointerWN.REGION) ?? FindLexical(wordId, PointerWN.REGION);
            if (resut == null)
                resut = FindSemantic(wordId, PointerWN.TOPIC) ?? FindLexical(wordId, PointerWN.TOPIC);
            if (resut == null)
                resut = FindSemantic(wordId, PointerWN.USAGE) ?? FindLexical(wordId, PointerWN.USAGE);
            return resut;

        }

        public ISenseLexicalResult Member(IWordID wordId)
        {
            if (wordId == null)
                throw new ArgumentNullException();

            var result = Find(wordId, PointerWN.REGIONMEMBER);
            if (result == null)
                result = Find(wordId, PointerWN.TOPICMEMBER);
            if (result == null)
                result = Find(wordId, PointerWN.USAGEMEMBER);
            return result;
        }

        private ISenseLexicalResult FindSemantic(IWordID id, PointerWN pointer)
        {
            SenseLexicalResult result = null;
            ISynset synset = _dictionary.GetSynset(id.SynsetID);
            List<ISynsetID> relatedSynsets = synset.GetRelatedSynsets(pointer);
            if (relatedSynsets != null)
            {
                List<ISynset> synsets = new List<ISynset>();
                foreach (var synId in relatedSynsets)
                {
                    var relatedSynset = _dictionary.GetSynset(synId);
                    if( relatedSynset != null)
                        synsets.Add(relatedSynset);
                }
                result = new SenseLexicalResult(id, synsets, pointer);
            }
            return result;
        }

        private ISenseLexicalResult FindLexical(IWordID id, PointerWN pointer)
        {
            SenseLexicalResult result = null;
            IWord word = _dictionary.GetWord(id);
            List<IWordID> relatedWords = word.GetRelatedWords(pointer);
            if (relatedWords != null)
            {
                List<IWord> words = new List<IWord>();
                foreach (var wordId in relatedWords)
                {
                    var relatedWord = _dictionary.GetWord(wordId);
                    if (relatedWord != null)
                        words.Add(relatedWord);
                }

                result = new SenseLexicalResult(id, words, pointer);
            }
            return result;
        }
        public IAlsoSee FindAlsosee(IWord word)
        {
            if (word == null)
                throw new ArgumentNullException();
            IAlsoSee alsoSee = null;
            var relatedWord = word.GetRelatedWords(PointerWN.ALSOSEE);

            if (relatedWord != null)
            {
                alsoSee = new AlsoSeeNode(word);
                foreach (var w in relatedWord)
                {
                    alsoSee.AddWord(_dictionary.GetWord(w));
                };
            }
            else
            {
                var related = word.Synset.GetRelatedSynsets(PointerWN.ALSOSEE);
                if (related != null)
                {
                    alsoSee = new AlsoSeeNode(word);
                    foreach (var element in related)
                    {
                        var syn = _dictionary.GetSynset(element);
                        foreach (var w in syn.GetWords())
                            alsoSee.AddWord(w);
                    }
                }
            }

            return alsoSee;

        }
        public static bool IsAccepted(PointerWN p)
        {
            return p == null ? false : p.Relation == RelationType.Both;
        }
    }
}
