﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    public interface IWordBuilder
    {
        IWord ToWord(ISynset synset);
        void AddVerbFrame(IVerbFrame frame);
        void AddRelatedWord(IPointer type, IWordID id);
    }
}
