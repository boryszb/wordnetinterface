﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core.ElementContracts;

namespace WordNetWebApi.Models
{
    public interface IWNRepository
    {
        WordSense GetWordWithRelations(string lemma, string pos, string pointerWN);
        IEnumerable<WordModel> GetWord(string lemma);
        WordModel GetWord(string lemma, string pos);
        IWordDefinition GetIndexWord(string lemma, string pos);
        IEnumerable<ILexFile> GetLexFiles();
        ILexFile GetLexFile(int num);
    }
}
