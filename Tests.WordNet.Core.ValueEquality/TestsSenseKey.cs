﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsSenseKey
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 == key2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLemmaAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("tes", 1, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 != key2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLexIDAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 2, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 != key2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentPOSAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.ADJECTIVE, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 != key2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentAdjSatAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, false, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 != key2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLexFileAttributeAreNotEqual()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_PERT);

            Assert.IsTrue(key1 != key2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1.Equals(key1));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 == key2 && key2 == key1);
        }

        [TestMethod]
        public void NonEqualsIsTransitive()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key3 = new SenseKey("test", 2, POS.NOUN, true, LexFile.NOUN_ACT);

            Assert.IsTrue(key1 == key2 && key2 != key3 && key1 != key3);

        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key3 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.IsTrue(key1 == key2 && key2 == key3 && key1 == key3);

        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferentHashValue()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 2, POS.NOUN, false, LexFile.ADJ_ALL);

            Assert.AreNotEqual(key1.GetHashCode(), key2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            SenseKey key1 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);
            SenseKey key2 = new SenseKey("test", 1, POS.NOUN, true, LexFile.ADJ_ALL);

            Assert.AreEqual(key1.GetHashCode(), key2.GetHashCode());
        }
    }
}
