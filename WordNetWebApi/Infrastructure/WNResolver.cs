﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using WordNetWebApi.Models;

namespace WordNetWebApi.Infrastructure
{
    public class WNResolver: IDependencyResolver, IDependencyScope
    {

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public void Dispose()
        {
            // in this case do nothing - not required
        }

        public object GetService(Type serviceType)
        {
            return serviceType == typeof(IWNRepository)
                ? new WNRepository()
                : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Enumerable.Empty<object>();
        }
    }
}