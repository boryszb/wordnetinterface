﻿using System;


namespace WordNet.Core.Util
{
    public static class Constants
    {
        public static readonly String[] LexicalIDNumbers = new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09" };
        public const int FileNumberMin = 0;
        public const int FileNumberMax = 99;
        public const int WordNumberMin = 1;
        public const int WordNumberMax = 255;
        public const int LexicalIDMin = 0;
        public const int LexicalIDMax = 15;
        public const int LegalOffsetMin = 0;
        public const int LegalOffsetMax = 99999999;
        public const int AdjectiveSatelliteNumber = 5;
    }

    internal static class Message
    {
        public const string NotValidOffset = @"'{0}' is not a valid offset; offsets must be in the closed range [0,99999999]";
        public const string IllegalLexicalFileNumber = @"'{0}' is an illegal lexical file number: Lexical file numbers must be in the closed range [0,99]";
        public const string IllegalWordNumber = @"'{0}' is an illegal word number: word numbers are in the closed range [1,255]";
        public const string IllegalLexicalID = @"'{0}' is an illegal lexical id: lexical ids are in the closed range [0,15]";
        public const string NullException = "Null argumet is not allowed";
        public const string OutOfRange = "One or more arguments are out of range";
        public const string GraphWithCycle = "graph has a cycle";
        public const string NotOneRoot = "graph is not a one rooted DAG";
        public const string NotInDictionary = "word not in dictionary";

    }
}
