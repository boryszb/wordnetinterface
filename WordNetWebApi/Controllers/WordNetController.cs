﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WordNet.Core.ElementContracts;
using WordNetWebApi.Models;

namespace WordNetWebApi.Controllers
{
    /// <summary>
    /// service for reading WordNet data
    /// </summary>
    [RoutePrefix("api/WordNet")]
    public class WordNetController : ApiController
    {

        readonly IWNRepository _repository;
        /// <summary>
        /// service for reading WordNet data
        /// </summary>
        public WordNetController()
        {
            _repository = (IWNRepository)GlobalConfiguration
                            .Configuration
                            .DependencyResolver
                            .GetService(typeof(IWNRepository));
        }
        /// <summary>
        /// Retrives information about all sense of a word for all its syntactic categories (Noun, Verb, Adjective, Adverb)
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <returns>array of json objects representing all sense of a word for each syntactic catergory</returns>
        [Route("ov/{word}")]
        public HttpResponseMessage Get(string word)
        {
            return DoGetWord(word);
        }

        /// <summary>
        /// Retrives information about all senses of a word for its noun syntactic category.
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <returns>array of json objects representing all sense of a word for its noun syntactic category</returns>
        [Route("noun/{word}")]
        public HttpResponseMessage GetNoun(string word)
        {
            return DoGetWordOfType(word, WNUtillity.NOUN);
        }

        /// <summary>
        /// Retrives information about lexical and semantic relations belonging to a word in the syntactic category of noun.
        /// pointers for noun:
        /// <para>antonym (noun/{word}/antonym), </para>
        /// <para>hypernym (noun/{word}/heypernym), </para>
        /// <para>hyponym (noun/{word}/hyponym), </para> 
        /// <para>attribute (noun/{word}/attribute), </para>
        /// <para>derivationallyrelated (noun/{word}/derivationallyrelated),</para>
        /// <para>domain (noun/{word}/domain),</para>
        /// <para>member (noun/{word}/member),</para>
        /// <para>memberholonym (noun/{word}/memberholonym),</para>
        /// <para>substanceholonym (noun/{word}/substanceholonym),</para>
        /// <para>partholonym (noun/{word}/partholonym),</para>
        /// <para>membermeronym (noun/{word}/membermeronym),</para>
        /// <para>substancemeronym (noun/{word}/substancemeronym),</para>
        /// <para>partmernonym (noun/{word}/partmeronym)</para>
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <param name="pointerWN">a semantic or lexical pointer; semantic pointers are between synsets; lexical pointers
        /// are relations between individual words in senysets;
        /// </param>
        /// <returns>returns an array of jason objects representing senses and its pointers</returns>
        [Route("noun/{word}/{pointerWN}")]
        public IHttpActionResult GetNoun(string word, string pointerWN)
        {
            return GetWordAndRelationsImpl(word, WNUtillity.NOUN, pointerWN);
        }

        /// <summary>
        ///  Retrives information about all senses of a word for its syntactic category of verb.
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <returns>a json objects representing all sense of a word for its syntactic category of verb</returns>
        [Route("verb/{word}")]
        public HttpResponseMessage GetVerb(string word)
        {
            return DoGetWordOfType(word, WNUtillity.VERB);
        }

        /// <summary>
        ///  Retrives information about lexical and semantic relations belonging to a word in the syntactic category of verb.
        /// pointers for verb:
        /// <para>antonym (verb/{word}/antonym),</para>
        /// <para>hypernym (verb/{word}antonym),</para>
        /// <para>hyponym (verb/{word}/hyponym),</para>
        /// <para>entailment (verb/{word}/entailment),</para>
        /// <para>cause (verb/{word}/cause),</para>
        /// <para>alsosee (verb/{word}/alsosee),</para>
        /// <para>derivationallyrelated (verb/{word}/derivationallyrelated),</para>
        /// <para>domain (verb/{word}/domain)</para>
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <param name="pointerWN">a semantic or lexical pointer; semantic pointers are between synsets; lexical pointers
        /// are relations between individual words in senysets;</param>
        /// <returns>returns an array of jason object representing senses and its pointers</returns>
        [Route("verb/{word}/{pointerWN}")]
        public IHttpActionResult GetVerb(string word, string pointerWN)
        {
            return GetWordAndRelationsImpl(word, WNUtillity.VERB, pointerWN);
        }

        /// <summary>
        ///  Retrives information about all senses of a word in the syntactic category of adjective.
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <returns>a json objects representing all sense of a word in the syntactic category of adjective</returns>
        [Route("adj/{word}")]
        public HttpResponseMessage GetAdj(string word)
        {
            return DoGetWordOfType(word, WNUtillity.ADJ);
        }

        /// <summary>
        /// Retrives information about lexical and semantic relations belonging to a word in the syntactic category of adjective.
        /// Pointers for adjective:
        /// <para>antonym (adj/{word}/antonym),</para>
        /// <para>similarto (adj/{word}/similarto),</para>
        /// <para>participle (adj/{word}/participle)</para>
        /// <para>pertainym (adj/{word}/pertainym),</para>
        /// <para>attribute (adj/{word}/attribute),</para>
        /// <para>alsosee (adj/{word}/alsosee),</para>
        /// <para>domain (adj/{word}/domain)</para>
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <param name="pointerWN">a semantic or lexical pointer; semantic pointers are between synsets; lexical pointers
        /// are relations between individual words in senysets;</param>
        /// <returns>returns an array of jason object representing senses and its pointers</returns>
        [Route("adj/{word}/{pointerWN}")]
        public IHttpActionResult GetAdj(string word, string pointerWN)
        {
            return GetWordAndRelationsImpl(word, WNUtillity.ADJ, pointerWN);
        }

        /// <summary>
        ///  Retrives information about all senses of a word in the syntactic category of adverb.
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <returns>a json objects representing all sense of a word in the syntactic category of adverb</returns>
        [Route("adv/{word}")]
        public HttpResponseMessage GetAdv(string word)
        {
            return DoGetWordOfType(word, WNUtillity.ADV);
        }

        /// <summary>
        ///  Retrives information about lexical and semantic relations belonging to a word in the syntactic category of adverb.
        ///  Pointers for adverb:
        /// <para>antonym (adv/{word}/antonym), </para>
        /// <para>derived from adjective (adv/{word}/derived), </para>
        /// <para>domain (adv/{word}/domain), </para>
        /// </summary>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <param name="pointerWN">a semantic or lexical pointer; semantic pointers are between synsets; lexical pointers
        /// are relations between individual words in senysets;</param>
        /// <returns>returns an array of jason object representing senses and its pointers</returns>
        [Route("adv/{word}/{pointerWN}")]
        public IHttpActionResult GetAdv(string word, string pointerWN)
        {
            return GetWordAndRelationsImpl(word, WNUtillity.ADV, pointerWN);
        }

        /// <summary>
        /// Gets the word definition from a index file.
        /// </summary>
        /// <param name="type">part od speach (noun, verb, adjective, adverb)</param>
        /// <param name="word">a search word; it can be a single word, hyphenated string, or a collocation</param>
        /// <returns>returns word definition from index file for the specified syntactic category</returns>
        [Route("index/{type}/{word}")]
        public IHttpActionResult GetIndex(string type, string word)
        {
            if (String.IsNullOrWhiteSpace(word) || String.IsNullOrWhiteSpace(type))
                return NotFound();

            IWordDefinition result;
            try
            {
                result = _repository.GetIndexWord(word, type);
            }
            catch (Exception ex)
            {
                HttpError error = CreateErrorMessage(ex.Message, new[] { "type", "word" }, new[] { type, word });
                return Content(HttpStatusCode.BadRequest, error);
            }

            return Content(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// Retrives the name of the lexicographer file using corresponding file number.
        /// </summary>
        /// <param name="number">two digit file number corresponding to a file name</param>
        /// <returns>json object containing information about the file name</returns>
        [Route("lexnames/{number}")]
        public IHttpActionResult GetLexName(int number)
        {
            if (number < 0 || number > 44)
            {
                HttpError error = CreateErrorMessage(WNUtillity.OutOfRange, new[] { "number" }, new[] { $"{number}" });
                return Content(HttpStatusCode.RequestedRangeNotSatisfiable, error);
            }

            ILexFile result;
            try
            {
                result = _repository.GetLexFile(number);
            }
            catch (Exception ex)
            {
                HttpError error = CreateErrorMessage(ex.Message, new[] { "number" }, new[] { $"{number}" });
                return Content(HttpStatusCode.BadRequest, error);
            }

            return Ok(result);
        }

        /// <summary>
        /// Retrives the list of WordNet lexicographer file names.
        /// </summary>
        /// <returns>array of json objects containing information about file names</returns>
        [Route("lexnames/all")]
        public IHttpActionResult GetAllLexNames()
        {
            IEnumerable<ILexFile> result;
            try
            {
                result = _repository.GetLexFiles();
            }
            catch (Exception ex)
            {
                HttpError error = CreateErrorMessage(ex.Message, Array.Empty<string>(), Array.Empty<string>());
                return Content(HttpStatusCode.BadRequest, error);
            }

            return Ok(result);
        }

        private HttpResponseMessage DoGetWordOfType(string word, string pos)
        {
            WordModel result;

            try
            {
                result = _repository.GetWord(word, pos);
            }
            catch (Exception ex)
            {
                HttpError error = CreateErrorMessage(ex.Message, new[] { "word", "pos" }, new[] { word, pos });
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error);
            }

            if (result == null)
            {
                HttpError error = CreateErrorMessage(WNUtillity.NotSuchWord, new[] { "word", "pos" }, new[] { word, pos });
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, error);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage DoGetWord(string word)
        {
            IEnumerable<WordModel> result;
            try
            {
                result = _repository.GetWord(word);
            }
            catch (Exception ex)
            {
                HttpError error = CreateErrorMessage(ex.Message, new[] { "word" }, new[] { word });
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, error);
            }

            if (result == null || !result.Any())
            {
                HttpError error = CreateErrorMessage(WNUtillity.NotSuchWord, new[] { "word" }, new[] { word });
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, error);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private IHttpActionResult GetWordAndRelationsImpl(string word, string pos, string pointerWN)
        {
            WordSense result;
            try
            {
                result = _repository.GetWordWithRelations(word, pos, pointerWN);
            }
            catch (Exception ex)
            {
                HttpError error = CreateErrorMessage(ex.Message,
                       new[] { "word", "pos", "pointerWN" }, new[] { word, pos, pointerWN });

                return Content(HttpStatusCode.BadRequest, error);
            }

            if (result == null)
            {
                HttpError error = CreateErrorMessage(WNUtillity.NotSuchWord,
                    new[] { "word", "pos", "pointerWN" }, new[] { word, pos, pointerWN });

                return Content(HttpStatusCode.NotFound, error);
            }

            if (!result.Relations.Any())
            {
                HttpError error = CreateErrorMessage(WNUtillity.NoRealation,
                    new[] { "word", "pos", "pointerWN" }, new[] { word, pos, pointerWN });

                return Content(HttpStatusCode.NotFound, error);
            }
            return Content(HttpStatusCode.OK, result);
        }
        private HttpError CreateErrorMessage(string message, string[] parameters, string[] vals)
        {
            HttpError error = new HttpError()
            {
                Message = message

            };
            for (int i = 0; i < parameters.Length; i++)
            {
                error.Add(parameters[i], vals[i]);
            }

            return error;
        }
    }
}
