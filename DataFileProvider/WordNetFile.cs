﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace DataFileProvider
{
    public sealed class WordNetFile: IDataSource
    {
        private readonly FileInfo _file;
        private Encoding _enc = Encoding.Default;
       
        public WordNetFile(FileInfo file)
        {
            _file = file ?? throw new ArgumentNullException(nameof(file));
            _enc = DetectEncoding();
        }

        private Encoding DetectEncoding()
        {
            Encoding enc = Encoding.Default;
            using (StreamReader encDetector = new StreamReader(_file.FullName, true))
            {
                enc = encDetector.CurrentEncoding;
            }
            return enc;
        }

        private int Compare (string key, BinaryReader reader)
        {
            if (key == null || key.Length == 0)
               throw new ArgumentOutOfRangeException(nameof(key));
            if (reader == null)
                throw new ArgumentNullException(nameof(reader));
            List<byte> lineData = new List<byte>(100);
          
            while(reader.PeekChar() != ' ')
            {
                lineData.Add(reader.ReadByte());
            }
           
            string readData = _enc.GetString(lineData.ToArray());

            return String.CompareOrdinal(readData, key);  
        }
        internal Encoding Enc { get => _enc; set => _enc = value; }
        
        public string Search(string word)
        {
            String line = null;
            int keyLength = _enc.GetByteCount(word);
            using (BinaryReader buffer = new BinaryReader(_file.OpenRead(), _enc))
            {
                long start = 0;
                long midpoint = -1;
                long stop = buffer.BaseStream.Length;
                int cmp;

                while (stop - start > 1)
                {
                    midpoint = (start + stop) / 2;
                    buffer.BaseStream.Seek(midpoint, SeekOrigin.Begin);
                    RewindToLineStart(buffer);

                    cmp = Compare(word, buffer);
                    if (cmp == 0)
                    {
                        long offset = buffer.BaseStream.Position - keyLength;
                        line = ReadLine(offset);
                        break;
                    }
                    if (cmp > 0)
                        stop = midpoint;
                    else
                        start = midpoint;
                }
            }
           return line;
        }

        public IEnumerable<string> AllLines()
        {
            const int FirstLine = 29;
                     
            if (_file.Name.Equals("index.sense", StringComparison.Ordinal))
                return File.ReadAllLines(_file.FullName);
           
            int skip = 0;
            if (_file.Name.StartsWith("data",StringComparison.Ordinal) || _file.Name.StartsWith("index",StringComparison.Ordinal))
                skip = FirstLine;

            return File.ReadLines(_file.FullName).Skip(skip);
        }

        public string Name
        {
            get { return _file.Name; }
        }

        public string ReadLine(long key)
        {
            string line = string.Empty;
            using (StreamReader reader = new StreamReader(_file.FullName, _enc))
            {
                reader.BaseStream.Seek(key, SeekOrigin.Begin);
                line = reader.ReadLine();
                //reader.Close();
            }
            return line;
        }

       internal static void RewindToLineStart(BinaryReader buf)
        {
            buf = buf ?? throw new ArgumentNullException(nameof(buf));
            
            long i = buf.BaseStream.Position;

                buf.BaseStream.Position = i - 1;
                //buf.Position = i - 1;
                if (buf.ReadChar() == '\r' && buf.ReadChar() == '\n')
                    i--;
                if (i > 0)
                    i--;
                char c;
                for (; i > 0; i--)
                {
                    buf.BaseStream.Seek(i, SeekOrigin.Begin);
                    c = buf.ReadChar();
                    if (c == '\n' || c == '\r')
                    {
                        //i++;
                        break;
                    }
                }
            }

        internal String SearchFirstLine(String word)
        {
            string line;
            using (var reader = _file.OpenText()) {
                line = reader.ReadLine();
            };

            if ( line.StartsWith($"{word} ",StringComparison.Ordinal))
            {
                return line;
            }
            
            return null;
        }

    }
 }

