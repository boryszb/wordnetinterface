﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    // contract for a word in the index file.
    // WordNet database contains index files for each syntactic category(noun, verb, adj, adv)
    // Each index file is an alphabetized list of all the words in WordNet in the corresponding part of speech.
    // On each line, following the word, is a list of byte offset in the corresponding data file, 
    // one for each synset containign the word. In other words, it is a list of pointers to the meanings
    // the word may bear.
    // A line in the index file has the following format:
    // lemma  pos  synset_cnt  p_cnt  [ptr_symbol...]  sense_cnt  tagsense_cnt   synset_offset  [synset_offset...]
    // where:
    // -lemma is lower case ASCII text word or collocation;
    // -pos is syntactic category;
    // -synset_cnt is number of synsets that lemma is in;
    // -p_cnt is a number of different pointers that lamma has in all synsets containing it;
    // -ptr_symbol a list of different types of pointers that lemma has in all synsets containing it;
    // -sense_cnt is the same as -synset_cnt;
    // -tagsense_cnt is a number of senses of lemma that are ranked according to their frequency of occurrence
    // in semantic texts;
    // -synset_offset is a byte offset in data file of a synset containing lemma. It corresponds to 
    // a specific sense of lemma in WordNet. It can be used to read a synset from the data file.

    // example: anglia n 1 1 @ 1 0 08745721
    public interface IIndexWord: IHasPOS, IItem<IIndexWordID>
    {
        String Lemma { get; }
        List<IWordID> GetWordIDs();
        int TagSenseCount { get; }
        HashSet<IPointer> GetPointers();
    }
}
