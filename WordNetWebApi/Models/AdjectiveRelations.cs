﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace WordNetWebApi.Models
{
    public class AdjectiveRelations: BaseRelations
    {
        public AdjectiveRelations(IWNDictionary dic, string rel) : base(dic, rel, new Dictionary<string, string>()
        {
            {"antonym","!"}, {"similarto","&"}, {"participle", "<"}, {"pertainym","\\"}, {"attribute", "="},
            {"alsosee", "^" }, {"domain", ";"}
        })
        { }
        public override POS TypeOfSpeach => POS.ADJECTIVE;
    }
}