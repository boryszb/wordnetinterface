﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsers;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace UnitTestWordNetParsers
{

    /* Adjective index file is an alphabetized list of all adjectives in WordNet. 
  * On each line, following the word, is a list of byte offsets in 
  * the corresponding data file, one for each synset containing the adjective.
  *
  */

    [TestClass]
    public class IndexAdjectiveTests
    {

        const string ADJECTIVE_LINE = "acknowledged a 2 4 ! & ^ = 2 0 00029047 01987237";
        private IParser<IIndexWord> Create()
        {
            return (IParser<IIndexWord>)Activator.CreateInstance(typeof(IndexLineParser), true);
        }

        [TestMethod]
        public void Parse_WithAdjectiveLine_ReturnsInstanceOfIIndexWord()
        {
            var parser = Create();
            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.IsInstanceOfType(result, typeof(IIndexWord));
        }

        [TestMethod]
        public void Parse_WithAdjectiveLine_ReturnsLemmaFiled()
        {
            const string EXPECTED_LEMA = "acknowledged";
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.AreEqual(EXPECTED_LEMA, result.Lemma);
        }

        [TestMethod]
        public void Parse_WithAdjectiveLine_ReturnsAdjectiveSyntacticCategory()
        {
            POS EXPECTED_POS = POS.ADJECTIVE;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.AreEqual(EXPECTED_POS, result.PartOfSpeech);
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingPointerList_ReturnsAllElementsOfTheList()
        {
            const int EXPECTED_ELEMENTS_NUMBER = 4;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.AreEqual(EXPECTED_ELEMENTS_NUMBER, result.GetPointers().Count);
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingAntonymField_ReturnsAntonymPointer()
        {
            var EXPECTED_POINTER_ANTONYM = PointerWN.ANTONYM;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_ANTONYM));
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingSimilarToField_ReturnsSimilarToPointer()
        {
            var EXPECTED_POINTER_SIMILARTO = PointerWN.SIMILARTO;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_SIMILARTO));
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingAlsoSeeField_ReturnsAlsoSeePointer()
        {
            var EXPECTED_POINTER_ALSOSEE = PointerWN.ALSOSEE;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_ALSOSEE));
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingAttributeField_ReturnsAttributePointer()
        {
            var EXPECTED_POINTER_ATTRIBUTE = PointerWN.ATTRIBUTE;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.IsTrue(result.GetPointers().Contains(EXPECTED_POINTER_ATTRIBUTE));
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingByteOffsetList_ReturnsWordIDsForAllOffsets()
        {
            const int EXPECTED_OFFSET_NUMBER = 2;
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.AreEqual(EXPECTED_OFFSET_NUMBER, result.GetWordIDs().Count);
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingByteOffsetList_ReturnsWordIDForFirstOffset()
        {
            int EXPECTED_OFFSET = int.Parse("00029047");
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[0].SynsetID.Offset);
        }

        [TestMethod]
        public void Parse_WithAdjectiveLineContainingByteOffsetList_ReturnsWordIDForSecondOffset()
        {
            int EXPECTED_OFFSET = int.Parse("01987237");
            var parser = Create();

            var result = parser.Parse(ADJECTIVE_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, result.GetWordIDs()[1].SynsetID.Offset);
        }
    }
}
