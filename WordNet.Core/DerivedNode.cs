﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;

namespace WordNet.Core
{
    public class DerivedNode : WordNodeBase, IDerived
    {
        private readonly IList<IWord> _list = new List<IWord>();
        public DerivedNode(IWord word)
        {
            base.Word = word;
        }
        public IWord DerivedFrom
        {
            get; set;
        }

        public IEnumerable<IWord> DerivedFromList => _list.Select(w => w);

        public void AddWord(IWord word) => _list.Add(word);
    }
}
