﻿using System;


namespace WordNet.Core.DataContract
{
    public interface IParser<T>
    {
        T Parse(String line);
    }
}
