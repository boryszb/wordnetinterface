﻿using System;
using System.Reflection;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;
using System.Globalization;

namespace WordNet.Core
{
    public class PointerWN : IPointer
    {
        public static readonly PointerWN ALSOSEE = new PointerWN("^", "Also See", RelationType.Both);
        public static readonly PointerWN ANTONYM = new PointerWN("!", "Antonym", RelationType.Lexical);
        public static readonly PointerWN ATTRIBUTE = new PointerWN("=", "Attribute", RelationType.Semantic);
        public static readonly PointerWN CAUSE = new PointerWN(">", "Cause", RelationType.Semantic);
        public static readonly PointerWN DERIVATIONALLYRELATED = new PointerWN("+", "Derivationally related form", "DerivationallyRelated", RelationType.Lexical);
        public static readonly PointerWN DERIVEDFROMADJ = new PointerWN("\\", "Derived from adjective", RelationType.Lexical);
        public static readonly PointerWN DOMAIN = new PointerWN(";", "Domain of synset (undifferentiated)", "Domain", RelationType.Both);
        public static readonly PointerWN ENTAILMENT = new PointerWN("*", "Entailment", RelationType.Semantic);
        public static readonly PointerWN HYPERNYM = new PointerWN("@", "Hypernym", RelationType.Semantic);
        public static readonly PointerWN HYPERNYMINSTANCE = new PointerWN("@i", "Instance hypernym", "Hypernym", RelationType.Semantic);
        public static readonly PointerWN HYPONYM = new PointerWN("~", "Hyponym", RelationType.Semantic);
        public static readonly PointerWN HYPONYMINSTANCE = new PointerWN("~i", "Instance hyponym", "HyponymInstance", RelationType.Semantic);
        public static readonly PointerWN HOLONYMMEMBER = new PointerWN("#m", "Member holonym", RelationType.Semantic);
        public static readonly PointerWN HOLONYMSUBSTANCE = new PointerWN("#s", "Substance holonym", RelationType.Semantic);
        public static readonly PointerWN HOLONYMPART = new PointerWN("#p", "Part holonym", RelationType.Semantic);
        public static readonly PointerWN MEMBER = new PointerWN("-", "Member of this domain (undifferentiated)", "Member", RelationType.Both);
        public static readonly PointerWN MERONYMMEMBER = new PointerWN("%m", "Member meronym", RelationType.Semantic);
        public static readonly PointerWN MERONYMSUBSTANCE = new PointerWN("%s", "Substance meronym", RelationType.Semantic);
        public static readonly PointerWN MERONYMPART = new PointerWN("%p", "Part meronym", RelationType.Semantic);
        public static readonly PointerWN PARTICIPLE = new PointerWN("<", "Participle", RelationType.Lexical);
        public static readonly PointerWN PERTAINYM = new PointerWN("\\", "Pertainym (pertains to nouns)", "Pertainym", RelationType.Lexical);
        public static readonly PointerWN REGION = new PointerWN(";r", "Domain of synset - REGION", "Domain", RelationType.Both);
        public static readonly PointerWN REGIONMEMBER = new PointerWN("-r", "Member of this domain - REGION", "Member", RelationType.Both);
        public static readonly PointerWN SIMILARTO = new PointerWN("&", "Similar To", RelationType.Semantic);
        public static readonly PointerWN TOPIC = new PointerWN(";c", "Domain of synset - TOPIC", "Domain", RelationType.Both);
        public static readonly PointerWN TOPICMEMBER = new PointerWN("-c", "Member of this domain - TOPIC", "Member", RelationType.Both);
        public static readonly PointerWN USAGE = new PointerWN(";u", "Domain of synset - USAGE", "Domain", RelationType.Both);
        public static readonly PointerWN USAGEMEMBER = new PointerWN("-u", "Member of this domain - USAGE", "Member", RelationType.Both);
        public static readonly PointerWN VERBGROUP = new PointerWN("$", "Verb Group", RelationType.Both);

        private static readonly Dictionary<String, PointerWN> pointerMap = new Dictionary<string, PointerWN>();
        private static readonly HashSet<PointerWN> pointerSet = new HashSet<PointerWN>();

        private readonly String _symbol;
        private readonly String _name;
        private readonly String _shortName;
        private readonly String _toString;
        private readonly RelationType _relation;

        private const String ambiguousSymbol = "\\";
        static PointerWN()
        {
            SetDictionary();
        }

        private PointerWN(String symbol, String name, RelationType relation)
        {
            _symbol = CheckString(symbol);
            _name = CheckString(name);
            _shortName = _name;
            _toString = name.ToLower(CultureInfo.InvariantCulture).Replace(' ', '_').Replace(",", "");
            _relation = relation;
        }

        private PointerWN(String symbol, String name, String shortName, RelationType relation)
        {
            _symbol = CheckString(symbol);
            _name = CheckString(name);
            _shortName = CheckString(shortName);
            _toString = name.ToLower(CultureInfo.InvariantCulture).Replace(' ', '_').Replace(",", "");
            _relation = relation;
        }
        protected static String CheckString(String str)
        {
            if (str == null)
                throw new ArgumentNullException(nameof(str));
            str = str.Trim();
            if (str.Length == 0)
                throw new ArgumentOutOfRangeException(nameof(str));
            return str;
        }

        public RelationType Relation
        {
            get { return _relation; }
        }

        public string Symbol
        {
            get { return _symbol; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string ShortName
        {
            get { return _shortName; }
        }
        public override string ToString()
        {
            return _toString;
        }

        private static void SetDictionary()
        {
            FieldInfo[] fields = typeof(PointerWN).GetFields(BindingFlags.Static | BindingFlags.Public);
            for (int i = 0; i < fields.Length; i++)
            {
                if (fields[i].FieldType == typeof(PointerWN))
                {
                    var temp = (PointerWN)fields[i].GetValue(null);
                    if (!pointerMap.ContainsKey(temp.Symbol))
                        pointerMap.Add(temp.Symbol, temp);
                }
            }
        }

        public static PointerWN GetPointerType(String symbol, POS pos)
        {
            pos = pos ?? throw new ArgumentNullException(nameof(pos));
            symbol = symbol ?? throw new ArgumentNullException(nameof(symbol));

            if (pos == POS.GetPartOfSpeech(DbPartOfSpeechType.Adv) && symbol.Equals(ambiguousSymbol, StringComparison.Ordinal))
                return DERIVEDFROMADJ;
            if (pos == POS.GetPartOfSpeech(DbPartOfSpeechType.Adj) && symbol.Equals(ambiguousSymbol, StringComparison.Ordinal))
                return PERTAINYM;
            PointerWN pointerType = pointerMap[symbol];
            if (pointerType == null)
                throw new ArgumentException("No pointer type corresponding to symbol '" + symbol + "'");
            return pointerType;
        }

        public static IEnumerable<PointerWN> Values()
        {
            return pointerSet;
        }
    }
}

