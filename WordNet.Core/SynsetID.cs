﻿using System;
using System.Globalization;
using System.Text;
using WordNet.Core.ElementContracts;
using WordNet.Core.Extensions;

namespace WordNet.Core
{
    public sealed class SynsetID : ISynsetID, IEquatable<SynsetID>
    {

        public const String synsetIDPrefix = "SID-";

        private readonly int _offset;
        private readonly POS _pos;

        public SynsetID(int offset, POS pos)
        {
            offset.CheckOffset();

            _offset = offset;
            _pos = pos ?? throw new ArgumentNullException();
        }
        public int Offset
        {
            get { return _offset; }
        }

        public POS PartOfSpeech
        {
            get { return _pos; }
        }

        public override int GetHashCode()
        {
            const int PRIME = 31;
            int result = 17;
            result = PRIME * result + _offset.GetHashCode();
            result = PRIME * result + _pos.GetHashCode();
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj is SynsetID other)
            {
                return Equals(other);
            }
            return false;
        }

        public bool Equals(SynsetID other)
        {
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, other))
                return true;

            return this._offset == other._offset &&
                this._pos == other._pos;
        }

        public static bool operator ==(SynsetID left, SynsetID right)
        {
            if ((object)left == null || (object)right == null)
            {
                return Object.Equals(left, right);
            }

            return left.Equals(right);
        }

        public static bool operator !=(SynsetID left, SynsetID right)
        {
            return !(left == right);
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder(14);
            sb.Append(synsetIDPrefix);
            sb.Append(_offset.ZeroFillOffset());
            sb.Append('-');
            sb.Append(Char.ToUpper(_pos.Tag, CultureInfo.InvariantCulture));
            return sb.ToString();
        }

        public static SynsetID ParseSynsetID(String value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            value = value.Trim();
            if (value.Length != 14)
                throw new ArgumentOutOfRangeException(nameof(value));

            if (!value.StartsWith("SID-", StringComparison.Ordinal))
                throw new ArgumentOutOfRangeException(nameof(value));

            // get offset
            int offset = int.Parse(value.Substring(4, 12), CultureInfo.InvariantCulture);

            // get pos
            char tag = Char.ToLower(value[13], CultureInfo.InvariantCulture);
            POS pos = POS.GetPartOfSpeech(tag);
            if (pos == null)
                throw new ArgumentException("unknown part of speech tag: " + tag);

            return new SynsetID(offset, pos);
        }
    }
}
