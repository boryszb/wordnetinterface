﻿using System;
using System.Collections.Generic;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public class WordBuilder: IWordBuilder
    {
        private readonly int _num;
        private readonly int _lexID;
        private readonly String _lemma;
        private readonly AdjectiveMarker _marker;
        private readonly Dictionary<IPointer, List<IWordID>> _relatedWords = new Dictionary<IPointer, List<IWordID>>();
        private readonly List<IVerbFrame> _verbFrames = new List<IVerbFrame>();
       
        public WordBuilder(int num, String lemma, int lexID, AdjectiveMarker marker)
        {
            _num = num;
            _lemma = lemma;
            _lexID = lexID;
            _marker = marker;
        }
        
        public IWord ToWord(ISynset synset)
        {
            return new Word(synset, _num, _lemma, _lexID, _marker, _verbFrames, _relatedWords);
        }

        public void AddVerbFrame(IVerbFrame frame)
        {
            if (frame == null)
                throw new ArgumentNullException();
            _verbFrames.Add(frame);
        }

        public void AddRelatedWord(IPointer type, IWordID id)
        {
            if (type == null)
                throw new ArgumentNullException();
            if (id == null)
                throw new ArgumentNullException();
            List<IWordID> words = _relatedWords.ContainsKey(type) ? _relatedWords[type] : null;
            if (words == null)
            {
                words = new List<IWordID>();
                _relatedWords.Add(type, words);
            }
            words.Add(id);
        }

        protected int LexId => _lexID;
    }
}
