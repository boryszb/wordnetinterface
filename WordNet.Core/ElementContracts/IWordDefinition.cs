﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    public interface IWordDefinition:IHasPOS
    {
        string Lemma { get; }
        IIndexWord IndexWord { get; }
        IEnumerable<ISynset> Synsets { get; }
        ISynset GetSynset(ISynsetID id);
        IEnumerable<IWord> Words { get; }
        IWord GetWord(IWordID id);
    }
}
