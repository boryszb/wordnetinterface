﻿using System;
using System.Collections.Generic;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public sealed class DataType<T> : MefContainer, IDataType<T>
    {
        public static readonly DataType<IIndexWord> INDEX = new DataType<IIndexWord>("Index", "index", "idx");
        public static readonly DataType<ISynset> DATA = new DataType<ISynset>("Data", "data", "dat");
        public static readonly DataType<IExceptionEntryProxy> EXCEPTION = new DataType<IExceptionEntryProxy>("Exception", "exception", "exc");
        public static readonly DataType<ISenseEntry> SENSE = new DataType<ISenseEntry>("Sense", "sense");

        private readonly String _name;
        private readonly ISet<String> _hints;
        private readonly IParser<T> _parser;

        private DataType(String userName, params String[] hints)
        {
            _name = userName;
            _hints = new HashSet<String>(hints);
            _parser = Container.GetExportedValue<IParser<T>>();
        }

        public IParser<T> GetParser()
        {
            return _parser;
        }

        public ISet<string> GetResourceKeywords()
        {
            return _hints;
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
