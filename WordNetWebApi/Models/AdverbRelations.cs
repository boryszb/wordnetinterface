﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace WordNetWebApi.Models
{
    public class AdverbRelations : BaseRelations
    {
        public AdverbRelations(IWNDictionary dic, string rel) : base(dic, rel, new Dictionary<string, string>()
        {
            {"antonym","!"}, {"derived", "\\" }, {"domain", ";"}
        })
        { }

        public override POS TypeOfSpeach => POS.ADVERB;

        public override WordSense FindRelations(string lemma)
        {
            if (LexicalRelationTracer.IsAccepted(PointerType))
            {
                return FindLexicalRelations(lemma);
            }

            return FindBothRelations(lemma);
        }
    }
}