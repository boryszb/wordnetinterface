﻿using System;
using System.Collections.Generic;


namespace WordNet.Core.DataContract
{
    public interface IDataMapper<T>
    {
        IEnumerable<T> GetAll(POS pos);
        T GetDefinition(string query);

        T GetByKey(object key);
        IDataProvider<T> DataProvider { get; }
    }
}
