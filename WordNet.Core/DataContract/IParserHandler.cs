﻿using System;

namespace WordNet.Core.DataContract
{
    public interface IParserHandler<T>
    {
        T Parse(string[] tokens, string line);
    }
}
