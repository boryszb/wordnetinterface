﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNetWebApi.Models
{
    public interface IWNMemberRepository
    {
        IEnumerable<MemberModel> GetMemberInfo(string lemma);
    }
}
