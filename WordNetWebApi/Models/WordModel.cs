﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    public class WordModel
    {
        private List<SynsetDefinition> _synsets;
        public WordModel()
        {
            _synsets = new List<SynsetDefinition>();
        }
        public string Lemma { get; set; }
        public string TypeOfWord { get; set; }
        public IEnumerable<SynsetDefinition> Synsets => _synsets.Select(r => r);
        public void AddSynsetDefinition(SynsetDefinition def) => _synsets.Add(def);
    }
}