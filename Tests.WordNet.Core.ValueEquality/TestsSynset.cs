﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace Tests.WordNet.Core.ValueEquality
{
    [TestClass]
    public class TestsSynset
    {
        [TestMethod]
        public void TwoDifferentInstancesWithTheSameAttributesAreEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, true, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, true, false, "this is test", builders2, ids2);

            Assert.IsTrue(synset1 == synset2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentSynsetIDAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1112, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, true, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, true, false, "this is test", builders2, ids2);

            Assert.IsTrue(synset1 != synset2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentLexFileAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.NOUN_ACT, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.NOUN_ANIMAL, false, false, "this is test", builders2, ids2);


            Assert.IsTrue(synset1 != synset2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentAdjSatAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, true, false, "this is test", builders2, ids2);


            Assert.IsTrue(synset1 != synset2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentGlossAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this istest", builders2, ids2);

            Assert.IsTrue(synset1 != synset2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentWordsAttributeAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, null)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);


            Assert.IsTrue(synset1 != synset2);

        }

        [TestMethod]
        public void TwoDifferentInstancesWithDifferentMapOfRelatedSynsetAreNotEqual()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(334, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);


            Assert.IsTrue(synset1 != synset2);

        }

        [TestMethod]
        public void EqualsIsReflexive()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);

            Assert.IsTrue(synset1.Equals(synset1));
        }

        [TestMethod]
        public void EqualsIsSymmetric()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);


            Assert.IsTrue(synset1 == synset2 && synset2 == synset1);
        }

        [TestMethod]
        public void EqualsIsTransitive()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id3 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders3 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids3 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);
            Synset synset3 = new Synset(id3, LexFile.ADJ_ALL, false, false, "this is test", builders3, ids3);

            Assert.IsTrue(synset1 == synset2 && synset2 == synset3 && synset1 == synset3);

        }

        [TestMethod]
        public void NonEqualsIsTransitive()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id3 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders3 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test_test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids3 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);
            Synset synset3 = new Synset(id3, LexFile.ADJ_ALL, false, false, "this is test", builders3, ids3);

            Assert.IsTrue(synset1 == synset2 && synset2 != synset3 && synset1 != synset3);

        }

        [TestMethod]
        public void InstancesWithTheSameAttributesHaveTheSameHashValue()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);


            Assert.AreEqual(synset1.GetHashCode(), synset2.GetHashCode());
        }

        [TestMethod]
        public void InstancesWithDifferentAttributesHaveDifferenHashValues()
        {
            SynsetID id1 = new SynsetID(1111, POS.ADJECTIVE);
            SynsetID id2 = new SynsetID(1111, POS.ADJECTIVE);

            List<IWordBuilder> builders1 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            List<IWordBuilder> builders2 = new List<IWordBuilder>
            {
                new WordBuilder(2, "test", 2, AdjectiveMarker.POSTNOMINAL),
                new WordBuilder(1, "nexttest", 1, AdjectiveMarker.PREDICATE)
            };

            Dictionary<IPointer, List<ISynsetID>> ids1 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.HYPONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            // changed pointer
            Dictionary<IPointer, List<ISynsetID>> ids2 = new Dictionary<IPointer, List<ISynsetID>>()
            {
                {PointerWN.HYPERNYM, new List<ISynsetID>(){new SynsetID(333, POS.NOUN), new SynsetID(444, POS.NOUN) } },
                {PointerWN.ANTONYM, new List<ISynsetID>(){new SynsetID(337, POS.ADJECTIVE), new SynsetID(4554, POS.ADJECTIVE) } },
            };

            Synset synset1 = new Synset(id1, LexFile.ADJ_ALL, false, false, "this is test", builders1, ids1);
            Synset synset2 = new Synset(id2, LexFile.ADJ_ALL, false, false, "this is test", builders2, ids2);


            Assert.AreNotEqual(synset1.GetHashCode(), synset2.GetHashCode());
        }

    }
}
