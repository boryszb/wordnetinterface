﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordNet.Core.ElementContracts;
using WordNet.Core.Relations;
namespace WordNet.Core
{
    public class AntonymNode : WordNodeBase, IAntonym
    {
        readonly List<ISynset> _relatedSynsets;
        IList<IAntonym> _antonyms;
        public AntonymNode(IWord wn)
        {
            base.Word = wn;
            _relatedSynsets = new List<ISynset>();
            _antonyms = new List<IAntonym>();
        }

        public IAntonym Antonym
        {
            get; set;
        }

        public IList<IAntonym> Antonyms
        {
            get { return new List<IAntonym>(_antonyms); }
            //set { _antonyms = value; }
        }

        public ICollection<ISynset> RelatedSynsets
        {
            get { return new List<ISynset>(_relatedSynsets); }
        }

        public void AddRelatedSynsets(ISynset synset)
        {
            _relatedSynsets.Add(synset);
        }

        public void ClearAntonyms()
        {
            _antonyms.Clear();
        }

        public void AddRange(IList<IAntonym> antonyms)
        {
            _antonyms = _antonyms.Concat(antonyms).ToList();
        }

        public IWord Indirect
        {
            get; set;
        }
    }
}
