﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WordNetWebApi.Models
{
    public class SenseModel
    {
        private IList<SenseModel> _list = new List<SenseModel>();
        public string Name { get; set; }
        public string Description { get; set; }
        public string Gloss { get; set; }
        public string Words { get; set; }
        public IEnumerable<SenseModel> RelatedTo => _list.Select(m => m);
        public void AddModel(SenseModel m) => _list.Add(m);
    }
}