﻿using System;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    public interface ISenseLexicalResult
    {
        String Lemma { get; }
        RelationType CurrentRelation { get; }
        ISynset[] RelatedSynsets();
        IWord[] RelatedWords();
        PointerWN CurrentPointer { get; }
    }
}
