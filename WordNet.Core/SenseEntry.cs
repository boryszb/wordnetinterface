﻿using System;
using WordNet.Core.ElementContracts;
using WordNet.Core.Extensions;

namespace WordNet.Core
{
    public sealed class SenseEntry: ISenseEntry, IEquatable<SenseEntry>
    {
        private readonly int _offset;
        private readonly int _num;
        private readonly int _count;
        private readonly ISenseKey _key;

        public SenseEntry(ISenseKey key, int offset, int num, int count)
        {
            key = key ?? throw new ArgumentNullException();
            offset.CheckOffset();

            _key = key;
            _offset = offset;
            _num = num;
            _count = count;
        }
        
        public ISenseKey SenseKey
        {
            get { return _key; }
        }

        public int Offset
        {
            get { return _offset; }
        }

        public int SenseNumber
        {
            get { return _num; }
        }

        public int TagCount
        {
            get { return _count; }
        }
        
        public POS PartOfSpeech
        {
            get { return _key.PartOfSpeech; }
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 17;
            result = prime * result + _count.GetHashCode();
            result = prime * result + _key.GetHashCode();
            result = prime * result + _num.GetHashCode();
            result = prime * result + _offset.GetHashCode();
            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj is SenseEntry other)
            {
                return Equals(other);
            }
            return false;
        }

        public bool Equals(SenseEntry other)
        {
            if (other == null)
            {
                return false;
            }

            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }

            return this.TagCount == other.TagCount &&
                this._num == other._num &&
                this._offset == other._offset &&
                this._key.Equals(other._key);

        }

        public static bool operator ==(SenseEntry first, SenseEntry second)
        {
            if ((object)first == null || (object)second == null)
            {
                return Object.Equals(first, second);
            }

            return first.Equals(second);
        }

        public static bool operator !=(SenseEntry first, SenseEntry second)
        {
            return !( first == second );
        }
    }
}
