﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core.ElementContracts;
using WordNet.Core.Extensions;

namespace WordNet.Core
{
    public sealed class Synset:ISynset, IEquatable<Synset>
    {
        private readonly ISynsetID _id;
        private readonly String _gloss;
        private readonly ILexFile _lexFile;
        private readonly List<IWord> _words;
        private readonly bool _isAdjSat;
        private readonly bool _isAdjHead;
        private readonly List<ISynsetID> _related;
        private readonly Dictionary<IPointer, List<ISynsetID>> _relatedMap;

        public Synset(ISynsetID id, ILexFile lexFile, bool isAdjSat, bool isAdjHead, String gloss,
            List<IWordBuilder> wordBuilders, Dictionary<IPointer, List<ISynsetID>> ids)
        {
            if (lexFile == null)
                throw new ArgumentNullException(nameof(lexFile));
            if (wordBuilders == null)
                throw new ArgumentNullException(nameof(wordBuilders));
            if (wordBuilders.Count == 0)
                throw new ArgumentOutOfRangeException(nameof(wordBuilders));
            if (isAdjSat && isAdjHead)
                throw new ArgumentOutOfRangeException(nameof(isAdjSat));
            if ((isAdjSat || isAdjHead) && lexFile.Number != 0)
                throw new ArgumentOutOfRangeException(nameof(lexFile));

            _id = id ?? throw new ArgumentNullException(nameof(id));
            _lexFile = lexFile;
            _gloss = gloss ?? throw new ArgumentNullException(nameof(gloss));
            _isAdjSat = isAdjSat;
            _isAdjHead = isAdjHead;

            // words
            List<IWord> words = new List<IWord>(wordBuilders.Count);
            foreach (IWordBuilder wordBuilder in wordBuilders)
                words.Add(wordBuilder.ToWord(this));
            _words = words;

            HashSet<ISynsetID> hiddenSet = null;
            Dictionary<IPointer, List<ISynsetID>> hiddenMap = null;
            // fill synset map
            if (ids != null)
            {
                hiddenSet = new HashSet<ISynsetID>();
                hiddenMap = new Dictionary<IPointer, List<ISynsetID>>(ids.Count,new PointerIndexKeyComparator());
                foreach (KeyValuePair<IPointer, List<ISynsetID>> entry in ids)
                {
                    if (entry.Value == null || entry.Value.Count == 0)
                        continue;
                    hiddenMap.Add(entry.Key, new List<ISynsetID>(entry.Value));
                    foreach (var val in entry.Value)
                        hiddenSet.Add(val);
                }
            }
            _related = (hiddenSet != null && hiddenSet.Count != 0) ? new List<ISynsetID>(hiddenSet) : new List<ISynsetID>();
            _relatedMap = (hiddenMap != null && hiddenMap.Count != 0) ? hiddenMap : null;
        }
        
       

        public int Offset
        {
            get { return _id.Offset; }
        }


        public ILexFile LexicalFile
        {
            get { return _lexFile; }
        } 

        public int TypeOfPOS
        {
            get
            {
                POS pos = PartOfSpeech;
                if (pos != POS.GetPartOfSpeech(DbPartOfSpeechType.Adj))
                    return pos.Number;
                return IsAdjectiveSatellite ? 5 : 3;
            }
        }
        
        public string Gloss
        {
            get { return _gloss; }
        }

        public List<IWord> GetWords()
        {
            return _words;
        }

        public IWord GetWord(int wordNumber)
        {
            return _words[wordNumber - 1];
        }

        public bool IsAdjectiveHead
        {
            get { return _isAdjHead; }
        }

        public bool IsAdjectiveSatellite
        {
            get { return _isAdjSat; }
        }

        public Dictionary<IPointer, List<ISynsetID>> GetRelatedMap()
        {
            return _relatedMap;
        }

        public List<ISynsetID> GetRelatedSynsets(IPointer ptr)
        {
            if (_relatedMap == null)
                return null;
            if (!_relatedMap.ContainsKey(ptr))
                return null;

            List<ISynsetID> result = _relatedMap[ptr];
            return result ?? new List<ISynsetID>(); 
        }

        public List<ISynsetID> GetRelatedSynsets()
        {
            return _related;
        }

        public ISynsetID ID
        {
            get { return _id; }
        }

        public override int GetHashCode()
        {
            int PRIME = 31;
            int result = 17;
            result = PRIME * result + _gloss.GetHashCode();
            result = PRIME * result + (_isAdjSat ? 1231.GetHashCode() : 1237.GetHashCode());
            result = PRIME * result + _id.GetHashCode();

            for (int i = 0; i < _words.Count; i++)
                result = PRIME * result + _words[i].GetHashCode();
            for (int i = 0; i < _related.Count; i++)
                result = PRIME * result + _related[i].GetHashCode();
            foreach (var key in _relatedMap.Keys)
                result = PRIME * result + key.GetHashCode();

            return result;
        }

        public override bool Equals(Object obj)
        {
            if (obj is Synset other)
            {
                return Equals(other);
            }
            return false;
        }

        public bool Equals(Synset other)
        {
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, other))
                return true;

            return this._id.Equals(other._id) &&
                this._words.SequenceEqual(other._words) &&
                this._gloss.Equals(other._gloss, StringComparison.Ordinal) &&
                this._isAdjSat == other._isAdjSat &&
                this._related.SequenceEqual(other._related) &&
                this._relatedMap.Keys.SequenceEqual(other._relatedMap.Keys);
        }

        public static bool operator ==(Synset left, Synset right)
        {
            if ((object)left == null || (object)right == null)
            {
                return Object.Equals(left, right);

            }
            return left.Equals(right);
        }

        public static bool operator !=(Synset left, Synset right)
        {
            return !(left == right);
        }
        public POS PartOfSpeech
        {
            get { return _id.PartOfSpeech; }
        }
    }
}
