﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.Graph
{
   
    // represents contract for finding shortest paths (number of edges) from
    // a source vertex
    public interface IShortestPath
    {
        /// <summary>
        /// returns number of edges in a shortes path
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        int DistanceTo(int vertex);
        bool HasPathTo(int vertex);
        IEnumerable<int> PathTo(int vertex);
    }
}
