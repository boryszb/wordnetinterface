﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.Graph
{
    public interface IASPath
    {
        int Length(int vertexOne, int vertexTwo);
        int Length(IEnumerable<int> v, IEnumerable<int> w);
        int Ancestor(int vertexOne, int vertexTwo);
        int Ancestor(IEnumerable<int> v, IEnumerable<int> w);
    }
}
