﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordNet.Core.ElementContracts
{
    // represents inflected form of a word and its base form;
    // each line contains an inflected form followed by base form.
    public interface IExceptionEntryID:IHasPOS
    {
        String SurfaceForm { get; }
    }
}
