﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WordNet.Core.ElementContracts
{
    // Contract for verb sentence  frames for word in a synset.
    // Frame is of the form: f_cnt   +   f_num  w_num  [ +   f_num  w_num...]
    // where 
    // - f_cnt is two digit number indicating the number of frames listed;
    // - f_num is a two digit frame number;
    // - w_num is a two digit hexadecimal number indicating the word in the synset that the frame applies to;
    
    public interface IVerbFrame
    {
        int Number { get; }
        String Template { get; }
        String InstantiateTemplate(String verb);
    }
}
