﻿using System;
using System.ComponentModel.Composition;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace Parsers
{
    [Export(typeof(IParser<IExceptionEntryProxy>))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ExceptionLineParser:IParser<IExceptionEntryProxy>
    {
        private ExceptionLineParser() { }

        public IExceptionEntryProxy Parse(string line)
        {
            if (line == null)
                throw new ArgumentNullException();
            string[] forms = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string[] trimmed = new string[forms.Length - 1];
            for (int i = 1; i < forms.Length; i++)
            {
                trimmed[i - 1] = forms[i].Trim();
            }
            return new ExceptionEntryProxy(forms[0].Trim(), trimmed);
        }
    }
}
