﻿using System;
using System.Globalization;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public sealed class ExceptionEntryID: IExceptionEntryID, IEquatable<ExceptionEntryID>
    {
        private readonly String _surfaceForm;
        private readonly POS _pos;

        public ExceptionEntryID(String surfaceForm, POS pos)
        {
            if (surfaceForm == null)
                throw new ArgumentNullException(nameof(surfaceForm));
            surfaceForm = surfaceForm.Trim();
            if (surfaceForm.Length == 0)
                throw new ArgumentOutOfRangeException(nameof(surfaceForm));
            // all exception entries are lower-case
            // this call also checks for null
            _surfaceForm = surfaceForm.ToLower(CultureInfo.InvariantCulture);
            _pos = pos ?? throw new ArgumentNullException(nameof(pos));
        }
       
        public string SurfaceForm
        {
            get { return _surfaceForm; }
        }

        public POS PartOfSpeech
        {
            get { return _pos; }
        }

        public override string ToString()
        {
            return $"EID-{_surfaceForm}-{_pos.Tag}";
        }

        public override int GetHashCode()
        {
            const int PRIME = 31;
            int result = 17;
            result = PRIME * result + _surfaceForm.GetHashCode();
            result = PRIME * result + _pos.GetHashCode();
            return result;
        }

        public override bool Equals(object obj)
        {
            if (obj is ExceptionEntryID other)
            {
                return Equals(other);
            }

            return false;
        }

        public bool Equals(ExceptionEntryID other)
        {
            if (other == null) return false;

            if (Object.ReferenceEquals(this, other))
                return true;

            return this.SurfaceForm.Equals(other.SurfaceForm, StringComparison.Ordinal) &&
                    this.PartOfSpeech.Equals(other.PartOfSpeech);
        }

        public static bool operator ==(ExceptionEntryID idOne, ExceptionEntryID idSecond)
        {
            if ((Object)idOne == null || (Object)idSecond == null)
                return Object.Equals(idOne, idSecond);

            return idOne.Equals(idSecond);
        }

        public static bool operator !=(ExceptionEntryID idOne, ExceptionEntryID idSecond)
        {
            return !(idOne == idSecond);
        }
        public static ExceptionEntryID ParseExceptionEntryID(String value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!value.StartsWith("EID-",StringComparison.Ordinal))
                throw new ArgumentOutOfRangeException(nameof(value));

            if (value[value.Length - 2] != '-')
                throw new ArgumentOutOfRangeException(nameof(value));

            POS pos = POS.GetPartOfSpeech(value[value.Length - 1]);

            return new ExceptionEntryID(value.Substring(4, value.Length - 2), pos);
        }
    }
}
