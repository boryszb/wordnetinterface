﻿using System;
using System.Collections.Generic;
using System.Linq;
using WordNet.Core.ElementContracts;

namespace WordNet.Core
{
    public class WordDefinition:IWordDefinition
    {
        private readonly IIndexWord _indexWord;
        private readonly Dictionary<ISynsetID, ISynset> _synsets;
        private readonly Dictionary<IWordID, IWord> _words;

        public WordDefinition(IIndexWord indexWord, Dictionary<ISynsetID, ISynset> synsets,
            Dictionary<IWordID, IWord> words)
        {
            _indexWord = indexWord;
            _synsets = synsets;
            _words = words;
        }
       
        public string Lemma
        {
            get { return _indexWord.Lemma; }
        }

        public IIndexWord IndexWord
        {
            get { return _indexWord; }
        }

        public IEnumerable<ISynset> Synsets
        {
            get { return _synsets.Values.Select( v => v); }
        }

        public ISynset GetSynset(ISynsetID id)
        {
            return _synsets[id];
        }

        public IEnumerable<IWord> Words
        {
            get { return _words.Values.Select(v => v); }
        }

        public IWord GetWord(IWordID id)
        {
            return _words[id];
        }

        public POS PartOfSpeech
        {
            get { return _indexWord.PartOfSpeech; }
        }
    }
}
