﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parsers;
using WordNet.Core;
using WordNet.Core.DataContract;
using WordNet.Core.ElementContracts;

namespace UnitTestWordNetParsers
{

    /*  Noun data file contains information corresponding to synsets with relational pointers resolved to synset offsets.
     *  Each line in the file correspondes to a synset that consists of a list of synonymous words or collocations 
     *  (eg. "fountain pen" , "take in" ), and pointers that describe 
     *  the relations between this synset and other synsets. 
     *  Two kinds of relations are recognized: lexical and semantic. 
     *  Lexical relations hold between word forms; semantic relations hold between word meanings.
     */
    [TestClass]
    public class DataNounTests
    {
        const string NOUN_DATA_LINE = @"02064081 05 n 03 dog 0 domestic_dog 0 Canis_familiaris 0 022 @ 02063374 n 0000 #m 02063873 n 0000 #m 07886597 n 0000 ~ 01306302 n 0000 ~ 02064724 n 0000 ~ 02064853 n 0000 ~ 02065264 n 0000 ~ 02065366 n 0000 ~ 02067096 n 0000 ~ 02083377 n 0000 ~ 02089732 n 0000 ~ 02090197 n 0000 ~ 02090349 n 0000 ~ 02090520 n 0000 ~ 02090668 n 0000 ~ 02090886 n 0000 ~ 02091012 n 0000 ~ 02091875 n 0000 ~ 02092204 n 0000 ~ 02092713 n 0000 ~ 02093356 n 0000 %p 02137696 n 0000 | a member of the genus Canis (probably descended from the common wolf) that has been domesticated by man since prehistoric times; occurs in many breeds; ""the dog barked all night"" ";

        private IParser<ISynset> Create()
        {
            return (IParser<ISynset>)Activator.CreateInstance(typeof(DataLineParser), true);
        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsSynsetType()
        {
            var dataLineParser = Create();
            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsInstanceOfType(synset, typeof(ISynset));
        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsSynsetOffset()
        {
            const int EXPECTED_OFFSET = 2064081;
            var dataLineParser = Create();
            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_OFFSET, synset.Offset);
        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsNounSyntacticCategory()
        {
            POS EXPECTED_POS = POS.NOUN;
            var dataLineParser = Create();
            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_POS, synset.PartOfSpeech);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingLexFileNum_ReturnsSynsetWithLexFileObject()
        {
            const string EXPECTED_NAME = "noun.animal";
            const string EXPECTED_CONTENT = "nouns denoting animals";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(EXPECTED_NAME == synset.LexicalFile.Name &&
                          EXPECTED_CONTENT == synset.LexicalFile.Description);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingListOfWords_ReturnsAllWords()
        {
            const int EXPECTED_NUM = 3;
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_NUM, synset.GetWords().Count);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingListOfWords_ReturnsFirstWord()
        {
            const string EXPECTED_WORD = "dog";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_WORD, synset.GetWord(1).Lemma);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingListOfWords_ReturnsSecondWord()
        {
            const string EXPECTED_WORD = "domestic_dog";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_WORD, synset.GetWord(2).Lemma);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingListOfWords_ReturnsThirdWord()
        {
            const string EXPECTED_WORD = "Canis_familiaris";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_WORD, synset.GetWord(3).Lemma);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingListOfRelatedSynsets_ReturnsALLRelatedSynsets()
        {
            const int EXPECTED_NUM = 22;
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_NUM, synset.GetRelatedSynsets().Count);
        }

        [TestMethod]
        public void Parse_WithNounLineContainingHypernymRelation_ReturnsHypernymKey()
        {
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedMap().ContainsKey(PointerWN.HYPERNYM));
        }

        [TestMethod]
        public void Parse_WithNounLineContainingHolonymMemberRelation_ReturnsHolonymMemberKey()
        {
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedMap().ContainsKey(PointerWN.HOLONYMMEMBER));
        }

        [TestMethod]
        public void Parse_WithNounLineContainingHyponymRelation_ReturnsHyponymKey()
        {
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedMap().ContainsKey(PointerWN.HYPONYM));
        }

        [TestMethod]
        public void Parse_WithNounLineContainingMeronymPartRelation_ReturnsMeronymPartKey()
        {
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedMap().ContainsKey(PointerWN.MERONYMPART));
        }

        [TestMethod]
        public void Parse_WithNounLineContainingHypernymSynsets_MapsSynsetstoTheKey()
        {
            int[] EXPECTED_SYNSETS = new int[] { 2063374 };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedSynsets(PointerWN.HYPERNYM)
                                 .Select(syn => syn.Offset)
                                 .SequenceEqual(EXPECTED_SYNSETS));

        }

        [TestMethod]
        public void Parse_WithNounLineContainingHolonymMemberSynsets_MapsSynsetstoTheKey()
        {
            int[] EXPECTED_SYNSETS = new int[] { 2063873, 07886597 };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedSynsets(PointerWN.HOLONYMMEMBER)
                                 .Select(syn => syn.Offset)
                                 .SequenceEqual(EXPECTED_SYNSETS));

        }

        [TestMethod]
        public void Parse_WithNounLineContainingHyponymSynsets_MapsSynsetstoTheKey()
        {
            int[] EXPECTED_SYNSETS = new int[] { 1306302, 2064724, 2064853, 2065264, 2065366, 2067096, 2083377,
                                                 2089732, 2090197, 2090349, 2090520, 2090668, 2090886, 2091012, 2091875,
                                                  2092204, 2092713, 2093356 };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedSynsets(PointerWN.HYPONYM)
                                 .Select(syn => syn.Offset)
                                 .SequenceEqual(EXPECTED_SYNSETS));

        }

        [TestMethod]
        public void Parse_WithNounLineContainingMenonymPartSynsets_MapsSynsetstoTheKey()
        {
            int[] EXPECTED_SYNSETS = new int[] { 2137696 };
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.IsTrue(synset.GetRelatedSynsets(PointerWN.MERONYMPART)
                                 .Select(syn => syn.Offset)
                                 .SequenceEqual(EXPECTED_SYNSETS));

        }

        [TestMethod]
        public void Parse_WithNounLine_ReturnsGloss()
        {
            const string EXPECTED_TEXT = @"a member of the genus Canis (probably descended from the common wolf) that has been domesticated by man since prehistoric times; occurs in many breeds; ""the dog barked all night""";
            var dataLineParser = Create();

            ISynset synset = dataLineParser.Parse(NOUN_DATA_LINE);

            Assert.AreEqual(EXPECTED_TEXT, synset.Gloss);

        }
    }
}
