﻿using System;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Relations
{
    public abstract class WordNodeBase : IWordNode
    {
        private IWord _wordNode;

        public IWord Word
        {
            get { return _wordNode; }
            protected set { _wordNode = value; }
        }

        public string WordLemma
        {
            get { return _wordNode.Lemma; ; }
        }
    }
}
