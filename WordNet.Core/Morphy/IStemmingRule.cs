﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordNet.Core;
using WordNet.Core.ElementContracts;

namespace WordNet.Core.Morphy
{
    public interface IStemmingRule: IHasPOS
    {
        String GetSuffix();
        
        String GetEnding();
        
        IEnumerable<String> GetSuffixIgnoreSet();

        String ApplyTo(String word);

        String AddSuffixToEnd(String word, String suffix);
    }
}
